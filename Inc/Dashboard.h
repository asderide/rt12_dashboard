/*
 * Dashboard.h
 *
 *  Created on: 31.05.2018
 *      Author: asder
 */

#ifndef DASHBOARD_H_
#define DASHBOARD_H_

int ReadGPIOExpand (void);

//void SetGroup(void);
//void SetMenu(void);
//extern volatile uint8_t counter_CAN_status;
//extern volatile uint8_t counter_CAN_parameter;
//
//typedef struct
//{
//	unsigned int		SendToCAN		:1;
//	unsigned int		valChanged		:1;
//	unsigned int		active			:1;
//	unsigned int		state			:1;
//	unsigned int 		exists			:1;
//	unsigned int		alive			:1;
//}input_status;
//
//typedef	union
//{
//	uint16_t		value			:16;
//	int32_t			value_32		:32;
//	float			value_f;
//}input_value;
//
//typedef struct
//{
//	uint8_t		min			:8;
//	uint16_t	max			:12;
//	uint16_t	Dashboard	:12;
//	uint16_t	VDCU		:12;
//	double		faktor;
//}min_max;
//
//input_value LED_brightness;
//
//union Input
//{
//	struct RT10_Status
//	{
//		input_status		Kl50;
//		input_status		Inv_Res;
//		input_status		VDCU_Res;
//		input_status		PedCal;
//		input_status		SuspCal;
//		input_status		SuspCalFront;
//		input_status		SuspCalRear;
//		input_status		Reserve;
//
//		input_value			DRS_v;
//		input_status		DRS;
//		input_status		SoC_Mode;
//		input_status		Para_OK;
//
//		input_status		IMD;
//		input_status		BMS;
//		input_status		BSPD;
//		input_status		IS;
//		input_status		SBD;
//		input_status		SBl;
//		input_status		SBr;
//		input_status		BOTS;
//		input_status		BMS_Temp;
//		input_status		BMS_UV;
//		input_status		BMS_OV;
//
//		input_value			SoC;
//
//		input_status		Reduce_SC;
//		input_status		Raise_SC;
//		input_status		Reduce_TV;
//		input_status		Raise_TV;
//		input_status		AT90_in5;
//		input_status		AT90_in6;
//
//		input_value			current;
//		input_value			voltage;
//		input_value			PreCharge;
//		input_status		preCharge;
//
//		input_status		Module_VCU;
//		input_status		Module_VDCU;
//		input_status		Module_MU_Front;
//		input_status		Module_MU_Rear;
//		input_status		Module_PU;
//		input_status		Module_BMS;
//
//		input_value			Light;
//		input_value			White_Light;
//	}translation;
//	input_status data[31];
//};
//
//union Status
//{
//	struct Parameter_Status
//	{
//		min_max			Control_Mode;
//		min_max			Rekup_current_max;
//		min_max			Rekup_torque_brake_max;
//		min_max			Rekup_torque_egas_max;
//		min_max			slipRef;
//		min_max			PT_T;
//		min_max			PID_P;
//		min_max			PID_I;
//		min_max			PID_D;
//		min_max			PID_N;
//		min_max			PID_Kb;
//		min_max			Mdiff_min;
//		min_max			Mdiff_max;
//		min_max			vel_low;
//		min_max			vel_high;
//		min_max			trq_low;
//		min_max			trq_high;
//		min_max			power_max;
//		min_max			torque_max;
//		min_max			Setup;
//	}translation;
//	min_max data[20];
//};
//
//struct Parameter
//{
//	uint8_t 		valChanged;
//	uint16_t	 	MinMax[4][22];
//	double			Faktor[22];
//	input_status 	ETK_Mode;
//	input_value		failure;
//};
//
//struct Sound_Select
//{
//	uint8_t Sound1;
//	uint8_t Sound2;
//	uint8_t Sound3;
//	uint8_t Sound4;
//	uint8_t Sound5;
//	uint8_t Sound6;
//	uint8_t Sound7;
//	uint8_t Sound8;
//	uint8_t Sound9;
//	uint8_t Sound10;
//};
//
//union Time
//{
//	struct time_elements
//	{
//		uint8_t weekday	:8;
//		uint8_t day		:8;
//		uint8_t month	:8;
//		uint8_t year	:8;
//		uint8_t hour	:8;
//		uint8_t min		:8;
//		uint8_t sec		:8;
//	}single_time;
//uint8_t time [7];
//};
//
////extern union		Input				RT10_input;
////extern union		Status				parameter;
////extern struct		Parameter			status;
////extern struct		Sound_Select		sound_select;
////extern union		Time				RTC;
////
////typedef union 		RT10_input			RT10_Input;
////typedef union	  	parameter			Parameter;
////typedef struct		status				Parameter_Status;
////typedef union 		RTC					RTC_Time;
//
////Globale Variablen f�r die CAN-Kommunikation
//
//union Dashboard_Status
//{
//	struct byteorder_Dashboard_Status
//	{
//		uint8_t Kl50_active				:1;
//		uint8_t	Inv_Res_active			:1;
//		uint8_t VDCU_Res_active			:1;
//		uint8_t PedCal_active			:1;
//		uint8_t SuspCal_active			:1;
//		uint8_t Reserve_active			:1;
//		uint8_t	DRS_active				:1;
//		uint8_t SoC_Mode_active			:1;
//		uint8_t IMD_active				:1;
//		uint8_t BMS_active				:1;
//		uint8_t BSPD_active				:1;
//		uint8_t IS_active				:1;
//		uint8_t SBD_active				:1;
//		uint8_t SBl_active				:1;
//		uint8_t SBr_active				:1;
//		uint8_t BOTS_active				:1;
//		uint8_t BMS_Temp_active			:1;
//		uint8_t BMS_UV_active			:1;
//		uint8_t BMS_OV_active			:1;
//		uint8_t SoC_Dashboard_value		:7;
//		uint8_t Reduce_SC_active		:1;
//		uint8_t Raise_SC_active			:1;
//		uint8_t Reduce_TV_active		:1;
//		uint8_t Raise_TV_active			:1;
//		uint8_t Para_Change_active		:1;
//		uint8_t	Reset_Distance_active	:1;
//	}translation;
//	uint8_t data[4];
//};
//
//union VCU_Status
//{
//	struct byteorder_VCU_Status
//	{
//		uint8_t 	PU_power					:1;			//PA0
//		uint8_t 	FMU_power					:1;			//PA1
//		uint8_t 	VDCU_power					:1;			//PA2
//		uint8_t 	DB_power					:1;			//PA3
//		uint8_t 	IMD_power					:1;			//PA5
//		uint8_t 	RMU_power					:1;			//PA6
//		uint8_t 	BMS_power					:1;			//PA7
//		uint8_t 	RTDS_power					:1;			//PB4
//		uint8_t 	WPr_power					:1;			//PB5
//		uint8_t 	BL_power					:1;			//PB7
//		uint8_t 	Luefter_links_power			:1;			//PC0
//		uint8_t 	INVr_power					:1;			//PC1
//		uint8_t 	Luefter_rechts_power		:1;			//PC2
//		uint8_t 	INVl_power					:1;			//PC3
//		uint8_t 	WPl_power					:1;			//PC5
//		uint8_t 	SL_power					:1;			//PD3
//		uint8_t 	DCDC_power					:1;			//PD7
//		uint8_t 	SC_power					:1;			//PE2
//		uint8_t 	DRS_power					:1;			//PE3
//		uint8_t 	ETK_links_power				:1;			//PE4
//		uint8_t 	ETK_rechts_power			:1;			//PE5
//		uint8_t 	AIRP_power					:1;			//PE6
//		uint8_t 	DL_power					:1;			//PE7
//		uint8_t 	Reserve_power				:1;			//PA4
//		uint16_t 	Voltage_LV_VCU				:16;		//PF1
//	}translation;
//	uint8_t data[5];
//};
//
//union VDCU_Status
//{
//	struct byteorder_VDCU_Status
//	{
//		uint8_t main_state				:3;
//		uint8_t cl50_alive				:1;
//		uint8_t egas_alive				:1;
//		uint8_t brake_alive				:1;
//		uint8_t inverter_left_alive		:1;
//		uint8_t inverter_right_alive	:1;
//		uint8_t steering_alive			:1;
//		uint8_t recuperation_on			:1;
//		uint8_t slipcontrol_on			:1;
//		uint8_t torquevectoring_on		:1;
//		uint8_t calibration_mode		:3;
//		uint8_t calibration_state		:2;
//	}translation;
//	uint8_t data[4];
//};
//
//union VDCU_Parameter
//{
//	struct byteorder_VDCU_Parameter
//	{
//		uint8_t TorqueMax			:8;
//		uint8_t TorqueMinEgas		:8;
//		uint8_t TorqueMinBreak		:8;
//		uint8_t VoltageMax			:8;
//		uint8_t VoltageMin			:8;
//		uint8_t CurrentMax			:8;
//		uint8_t CurrentMin			:8;
//		uint8_t PowerMax			:8;
//	}translation;
//	uint8_t data[8];
//};
//
//union VDCU_Electrical
//{
//	struct byteorder_VDCU_Electrical
//	{
//		uint16_t	voltage_inv_left	:10;
//		uint16_t	voltage_inv_right	:10;
//		uint16_t	current_inv_left	:11;
//		uint16_t	current_inv_right	:11;
//		uint16_t	power_actual		:10;
//	}translation;
//	uint8_t data[7];
//};
//
//union VDCU_Wheelspeeds
//{
//	struct byteorder_VDCU_Wheelspeeds
//	{
//		uint16_t front_left		:16;
//		uint16_t front_right	:16;
//		uint16_t rear_left		:16;
//		uint16_t rear_right		:16;
//	}translation;
//	uint8_t data[8];
//};
//
//union VDCU_Velocity
//{
//	struct byteorder_VDCU_Velocity
//	{
//		uint16_t	velocity	:12;
//		uint16_t	distance	:16;
//	}translation;
//	uint8_t data[4];
//};
//
//union VDCU_Derating
//{
//	struct byteorder_VDCU_Derating
//	{
//		uint8_t		derating_factor				:7;
//		uint8_t 	derating_voltage_active		:1;
//		uint8_t 	derating_temp_active		:1;
//		uint8_t 	derating_current_active		:1;
//		uint8_t 	derating_speed_active		:1;
//		uint8_t 	derating_celltemp_active	:1;
//		uint8_t 	derating_invtemp_active		:1;
//		uint8_t 	undervolt_active			:1;
//		uint16_t 	idc_max						:11;
//	}translation;
//	uint8_t data[4];
//};
//
//union VDCU_Drivetrain
//{
//	struct byteorder_VDCU_Drivetrain
//	{
//		uint8_t	Inv_r_Temp		:8;
//		uint8_t Inv_l_Temp		:8;
//		uint8_t Mot_r_Temp		:8;
//		uint8_t Mot_l_Temp		:8;
//		uint8_t Inv_r_State		:4;
//		uint8_t Inv_l_State		:4;
//	}translation;
//	uint8_t data[5];
//};
//
//union AMS_Voltage
//{
//	struct byteorder_AMS_Voltage
//	{
//		uint16_t 	ams_max_volt		:16;
//		uint8_t 	ams_stack_max_volt	: 4;
//		uint8_t 	ams_cell_max_volt	: 4;
//		uint16_t 	ams_min_volt		:16;
//		uint8_t		ams_stack_min_volt	: 4;
//		uint8_t		ams_cell_min_volt	: 4;
//		uint16_t	ams_accu_ges_volt	:16;
//	}translation;
//	uint8_t data[8];
//};
//
//union AMS_Temp
//{
//	struct byteorder_AMS_Temp
//	{
//		uint8_t		ams_max_temp		: 8;
//		uint8_t		ams_stack_max_temp	: 4;
//		uint8_t		ams_sensor_max_temp	: 4;
//		uint8_t		ams_min_temp		: 8;
//		uint8_t		ams_stack_min_temp	: 4;
//		uint8_t		ams_sensor_min_temp	: 4;
//		uint8_t		ams_stack_ave_temp	: 8;
//	}translation;
//	uint8_t data[5];
//};
//
//union PU_Throttle
//{
//	struct byteorder_PU_Throttle
//	{
//		uint16_t	egas	:16;
//		uint16_t	gas1	:16;
//		uint16_t	gas2	:16;
//	}translation;
//	uint8_t data[6];
//};
//
//union PU_Brake
//{
//	struct byteorder_PU_Brake
//	{
//		uint16_t	value	:16;
//	}translation;
//	uint8_t data[2];
//};
//
//union Pedal_Unit
//{
//	struct byteorder_Pedalerie_Status
//	{
//		uint8_t bspd_ped				:1;
//		uint8_t BOTS					:1;
//		uint8_t gas_inplau				:1;
//		uint8_t br_gas_inplau			:1;
//		uint8_t Tor_Enc_cal				:1;
//		uint8_t pedal_cal_off			:1;
//	}translation;
//	uint8_t data [1];
//};
//
//union MU_Suspension
//{
//	struct byteorder_MU_Suspension
//	{
//		uint16_t	left	:16;
//		uint16_t	right	:16;
//	}translation;
//	uint8_t data[4];
//};
//
//union MU_Steering
//{
//	struct byteorder_MU_Steering
//	{
//		uint16_t	value	:16;
//	}translation;
//	uint8_t data[2];
//};
//
//union MU_TCool
//{
//	struct byteorder_MU_TCool
//	{
//		uint16_t	value	:16;
//	}translation;
//	uint8_t data[2];
//};
//
//union MU_Brake
//{
//	struct byteorder_MU_Brake
//	{
//		uint16_t	pressure	:16;
//	}translation;
//	uint8_t data[2];
//};
//
//union MU_Temp
//{
//	struct byteorder_MU_Temp
//	{
//		uint16_t	temp1	:16;
//		uint16_t	temp2	:16;
//		uint16_t	temp3	:16;
//	}translation;
//	uint8_t data[6];
//};
//
//union Measurement_Unit
//{
//	struct byteorder_Messbox_Status
//	{
//		uint8_t susp_cal_off			:1;
//		uint16_t calibration_counter	:15;
//		uint8_t SB_links				:1;
//		uint8_t SB_rechts				:1;
//	}state;
//	uint8_t data[3];
//};
//
//union Control_Mode
//{
//	struct byteorder_Control_Mode
//	{
//		uint8_t	rekup_active			:1;
//		uint8_t	SC_active				:1;
//		uint8_t TC_active				:1;
//	}translation;
//	uint8_t data [1];
//};
//
//union Rekuperation
//{
//	struct byteorder_Rekuperation
//	{
//		uint8_t current_max				:6;
//		uint8_t torque_max_brake		:5;
//		uint8_t torque_max_egas			:4;
//	}translation;
//	uint8_t data [2];
//};
//
//union Slip_Control
//{
//	struct byteorder_Slip_Control
//	{
//		uint8_t slipRef					:7;
//		uint8_t PT_T					:5;
//		uint16_t PID_P					:9;
//		uint16_t PID_I					:10;
//		uint16_t PID_D					:10;
//		uint8_t PID_N					:7;
//		uint8_t PID_Kb					:7;
//	}translation;
//	uint8_t data [7];
//};
//
//union Torque_Control
//{
//	struct byteorder_Torque_Control
//	{
//		uint8_t Mdiff_max				:4;
//		uint8_t Mdiff_min				:4;
//		uint8_t velocity_low			:7;
//		uint8_t velocity_high			:7;
//		uint8_t trq_low					:6;
//		uint8_t trq_high				:6;
//	}translation;
//	uint8_t data [5];
//};
//
//union Vehicle_Power
//{
//	struct byteorder_Vehicle_Power
//	{
//		uint8_t power_max				:7;
//		uint8_t torque_max				:6;
//	}translation;
//	uint8_t data [2];
//};
//
//union Setup_Select
//{
//	struct byteorder_Setup_Select
//	{
//		uint8_t Setup_1					:1;
//		uint8_t Setup_2					:1;
//		uint8_t Setup_3					:1;
//		uint8_t Setup_4					:1;
//		uint8_t Setup_5					:1;
//		uint8_t Setup_6					:1;
//		uint8_t Setup_7					:1;
//		uint8_t Setup_8					:1;
//		uint8_t Setup_9					:1;
//		uint8_t Setup_10				:1;
//		uint8_t Setup_11				:1;
//		uint8_t Setup_12				:1;
//		uint8_t Setup_13				:1;
//		uint8_t Setup_14				:1;
//		uint8_t Setup_15				:1;
//		uint8_t Setup_16				:1;
//	}translation;
//	uint8_t data[2];
//};
//
//union DCDC_Temp
//{
//	struct byteorder_DCDC_Temp
//	{
//		uint16_t	temp1	:16;
//		uint16_t	temp2	:16;
//		uint16_t	temp3	:16;
//		uint16_t	temp4	:16;
//	}translation;
//	uint8_t data[8];
//};
//
//extern union 	VDCU_Status			VDCU_Status;
//extern union	VDCU_Velocity		VDCU_Velocity;
//extern union 	VDCU_Electrical		VDCU_Electrical;
//extern union	VDCU_Wheelspeeds	VDCU_Wheelspeeds;
//extern union	VDCU_Velocity		VDCU_Velocity;
//extern union	VDCU_Derating		VDCU_Derating;
//extern union	VDCU_Drivetrain		VDCU_Drivetrain;
//extern union	VDCU_Parameter		VDCU_Parameter;
//extern union	Control_Mode		VDCU_Control_Mode;
//extern union	Rekuperation		VDCU_Rekuperation;
//extern union	Slip_Control		VDCU_Slip_Control;
//extern union	Torque_Control		VDCU_Torque_Control;
//extern union	Vehicle_Power		VDCU_Vehicle_Power;
//extern union	Setup_Select		VDCU_Setup_Select;
//extern union	AMS_Voltage			AMS_Voltage;
//extern union	AMS_Temp			AMS_Temp;
//extern union	AMS_Current			AMS_Current;
//extern union	PU_Throttle			PU_Throttle;
//extern union 	PU_Brake			PU_Brake;
//extern union	Pedal_Unit			Pedalerie;
//extern union	MU_Suspension		MU_Federweg_vorne;
//extern union	MU_Suspension		MU_Federweg_hinten;
//extern union 	MU_Steering			MU_Lenkwinkel;
//extern union	MU_TCool			MU_Kuehlertemp;
//extern union	MU_Brake			MU_Bremsdruck_vorne;
//extern union	MU_Brake			MU_Bremsdruck_hinten;
//extern union	MU_Temp				MU_Temperaturen_vorne;
//extern union	MU_Temp				MU_Temperaturen_hinten;
//extern union	Measurement_Unit	Messbox_vorne;
//extern union	Measurement_Unit	Messbox_hinten;
//extern union	Control_Mode		Control_Mode;
//extern union	Rekuperation		Rekuperation;
//extern union	Slip_Control		Slip_Control;
//extern union	Torque_Control		Torque_Control;
//extern union	Vehicle_Power		Vehicle_Power;
//extern union	Setup_Select		Setup_Select;
//extern union	VCU_Status			VCU_Status;
//extern union	DCDC_Temp			DCDC_Temperaturen;
//extern union 	Dashboard_Status	Dashboard_Status;
//
//
//typedef struct Display_Information
//{
//	uint8_t active					:1;
//	uint8_t SendToCan				:1;
//}display_output;
//
//typedef struct Display_Timer
//{
//	uint16_t timer_refresh_value	:14;
//}display_timer;
//
//struct Menu_Select
//{
//	display_output Idle_Mode;					// Verwendet f�r Dashboard V2.2 mit 132 Schalterstellungen
//	display_output Control_Mode;				// Verwendet f�r Dashboard V2.2 mit 132 Schalterstellungen
//	display_output Rekuperation;				// Verwendet f�r Dashboard V2.2 mit 132 Schalterstellungen
//	display_output Rekuperation_current_max;
//	display_output Rekuperation_torque_brake_max;
//	display_output Rekuperation_torque_egas_max;
//	display_output Schlupfkontrolle;			// Verwendet f�r Dashboard V2.2 mit 132 Schalterstellungen
//	display_output slipRef;
//	display_output PT_T;
//	display_output PID_P;
//	display_output PID_I;
//	display_output PID_D;
//	display_output PID_N;
//	display_output PID_Kb;
//	display_output Momentensteuerung;			// Verwendet f�r Dashboard V2.2 mit 132 Schalterstellungen
//	display_output Mdiff_max;
//	display_output Mdiff_min;
//	display_output vel_low;
//	display_output vel_high;
//	display_output trq_low;
//	display_output trq_high;
//	display_output Fahrzeugleistung;			// Verwendet f�r Dashboard V2.2 mit 132 Schalterstellungen
//	display_output Power_max;
//	display_output Torque_max;
//	display_output Setupauswahl;				// Verwendet f�r Dashboard V2.2 mit 132 Schalterstellungen
//	display_output Fahrzeugstatus;				// Verwendet f�r Dashboard V2.2 mit 132 Schalterstellungen
//	display_output Fahrzeugstatus_2;			// Verwendet f�r Dashboard V2.2 mit 132 Schalterstellungen
//	display_output Dashboard_Setup;
//	display_output BMS;
//	display_output Tractive_System;
//	display_output VDCU;
//	display_output Derating;
//	display_output Musikauswahl;				// Verwendet f�r Dashboard V2.2 mit 132 Schalterstellungen
//
//	display_timer display_status;
//	display_timer display_parameter;
//};
//
//
//extern struct Menu_Select			Display_Menu;
//
//enum Display_Menu
//{
//	Nr_Idle_Mode			= 0,
//	Nr_Control_Mode			= 1,
//	Nr_Rekuperation			= 2,
//	Nr_Schlupfkontrolle		= 3,
//	Nr_Momentensteuerung	= 4,
//	Nr_Fahrzeugleistung		= 5,
//	Nr_Setupauswahl			= 6,
//	Nr_Fahrzeugstatus_1		= 7,
//	Nr_Fahrzeugstatus_2		= 8,
//	Nr_Musikauswahl			= 9,
//	Nr_Dashboard_Setup		= 10
//};
//
//enum Parameternummerierung
//{
//	Nr_Reglerstufe				= 0,
//	Nr_current_min				= 1,
//	Nr_MaxRecTrqBrake			= 2,
//	Nr_MaxRecTrqEgas			= 3,
//	Nr_slipRef					= 4,
//	Nr_PT_T						= 5,
//	Nr_PID_P					= 6,
//	Nr_PID_I					= 7,
//	Nr_PID_D					= 8,
//	Nr_PID_N					= 9,
//	Nr_PID_Kb					= 10,
//	Nr_Mdiff_max				= 11,
//	Nr_Mdiff_min				= 12,
//	Nr_vel_low					= 13,
//	Nr_vel_high					= 14,
//	Nr_trq_low					= 15,
//	Nr_trq_high					= 16,
//	Nr_power_max				= 17,
//	Nr_torque_max				= 18,
//	Nr_Setup					= 19
//};
//
//enum MinMax
//{
//	Nr_Min						= 0,
//	Nr_Dashboard				= 1,
//	Nr_Max						= 2,
//	Nr_VDCU						= 3
//};
//
//enum DisplayCharacters
//{
//	Nr_Strich_rechts			= 0,
//	Nr_Strich_oben				= 1,
//	Nr_Grad						= 2,
//	Nr_Doppelstrich_rechts		= 3,
//	Nr_Dreifachstrich_rechts 	= 4,
//	Nr_Vierfachstrich_rechts 	= 5
//};

#define LED_ON					0x01					//LED anschalten
#define LED_OFF					0x00					//LED ausschalten




#endif /* DASHBOARD_H_ */
