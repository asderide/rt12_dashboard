/*
 * RTC.h
 *
 *  Created on: 20.06.2018
 *      Author: asder
 */

#ifndef RTC_H_
#define RTC_H_

#include "dashboard.h"

//define register adresses

//control registers
#define RTC_Control_1_addr			0x00		//Control_1
#define RTC_Control_2_addr			0x01		//Control_2
#define RTC_Control_3_addr			0x02		//Control_3

//time and date registers
#define RTC_seconds_addr			0x03		//Adresse f�r Sekunden-Register
#define RTC_minutes_addr			0x04		//Adresse f�r Minuten-Register
#define RTC_hours_addr				0x05		//Adresse f�r Stunden-Register
#define RTC_days_addr				0x06		//Adresse f�r Tage-Register
#define RTC_weekdays_addr			0x07		//Adresse f�r Wochentage-Register
#define RTC_months_addr				0x08		//Adresse f�r Monate-Register
#define RTC_years_addr				0x09		//Adresse f�r Jahre-Register

//alarm registers
#define RTC_minute_alarm_addr		0x0A		//Adresse f�r Minutenalarm-Register
#define RTC_hour_alarm_addr			0x0B		//Adresse f�r Stundenalarm-Register
#define RTC_day_alarm_addr			0x0C		//Adresse f�r Tagesalarm-Register
#define RTC_weekday_alarm_addr		0x0D		//Adresse f�r Wochentagesalarm-Register

//offset register
#define RTC_offset_addr				0x0E		//Adresse f�r Offset Register

//clockout and timer registers
#define RTC_Tmr_CLKOUT_ctrl_addr	0x0F		//Adresse f�r Timer Clockout Steuerung
#define RTC_Tmr_A_freq_ctrl_addr	0x10		//Adresse f�r Timer 'A' Frequenz-Steuerung
#define RTC_Tmr_A_reg_addr			0x11		//Adresse f�r Timer 'A'
#define RTC_Tmr_B_freq_ctrl_addr	0x12		//Adresse f�r Timer 'B' Frequenz-Steuerung
#define RTC_Trm_B_reg_addr			0x13		//Adresse f�r Timer 'B'

//Funktionsprototypen
void initRTC(void);
uint8_t getRTCsec 		(void);
uint8_t getRTCmin 		(void);
uint8_t getRTChour 		(void);
uint8_t getRTCday		(void);
uint8_t getRTCweekday	(void);
uint8_t getRTCmonth		(void);
uint8_t getRTCyear		(void);
void getRTCvalue		(void);
void sendRTCvalue		(uint8_t weekday, uint8_t day, uint8_t month, uint8_t year, uint8_t hour, uint8_t minute, uint8_t second);

enum weekday
{
	Sunday 		= 0x00,
	Monday 		= 0x01,
	Tuesday 	= 0x02,
	Wednesday 	= 0x03,
	Thursday 	= 0x04,
	Friday 		= 0x05,
	Saturday 	= 0x06
};

enum month
{
	January 	= 0x01,
	February 	= 0x02,
	March 		= 0x03,
	April		= 0x04,
	May			= 0x05,
	June		= 0x06,
	July		= 0x07,
	August		= 0x08,
	September	= 0x09,
	October		= 0x0A,
	November	= 0x0B,
	December	= 0x0C
};


#endif /* RTC_H_ */
