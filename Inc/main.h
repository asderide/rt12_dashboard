/**
  ******************************************************************************
  * File Name          : main.hpp
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* Includes ------------------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define LR3_Pin GPIO_PIN_2
#define LR3_GPIO_Port GPIOE
#define LR4_Pin GPIO_PIN_3
#define LR4_GPIO_Port GPIOE
#define LR5_Pin GPIO_PIN_4
#define LR5_GPIO_Port GPIOE
#define Button_best_Pin GPIO_PIN_5
#define Button_best_GPIO_Port GPIOE
#define Button_Start_Pin GPIO_PIN_6
#define Button_Start_GPIO_Port GPIOE
#define Debounce_RS_Int_Pin GPIO_PIN_13
#define Debounce_RS_Int_GPIO_Port GPIOC
#define LED2_Pin GPIO_PIN_0
#define LED2_GPIO_Port GPIOC
#define LED1_Pin GPIO_PIN_1
#define LED1_GPIO_Port GPIOC
#define Debounce_DK_en_Pin GPIO_PIN_0
#define Debounce_DK_en_GPIO_Port GPIOA
#define CS_LED_Pin GPIO_PIN_2
#define CS_LED_GPIO_Port GPIOA
#define CS_GPIO_Pin GPIO_PIN_3
#define CS_GPIO_GPIO_Port GPIOA
#define CS_RTC_Pin GPIO_PIN_4
#define CS_RTC_GPIO_Port GPIOA
#define DK4_Pin GPIO_PIN_0
#define DK4_GPIO_Port GPIOB
#define DK3_Pin GPIO_PIN_1
#define DK3_GPIO_Port GPIOB
#define DK2_Pin GPIO_PIN_2
#define DK2_GPIO_Port GPIOB
#define DK1_Pin GPIO_PIN_7
#define DK1_GPIO_Port GPIOE
#define IMD_Pin GPIO_PIN_11
#define IMD_GPIO_Port GPIOE
#define BMS_Pin GPIO_PIN_12
#define BMS_GPIO_Port GPIOE
#define BSPD_Pin GPIO_PIN_13
#define BSPD_GPIO_Port GPIOE
#define Debounce_DK_int_Pin GPIO_PIN_14
#define Debounce_DK_int_GPIO_Port GPIOE
#define RTC_CLK_out_Pin GPIO_PIN_12
#define RTC_CLK_out_GPIO_Port GPIOB
#define RTC_Time_Set_Pin GPIO_PIN_13
#define RTC_Time_Set_GPIO_Port GPIOB
#define RTC_Int_Pin GPIO_PIN_14
#define RTC_Int_GPIO_Port GPIOB
#define LCD0_Pin GPIO_PIN_8
#define LCD0_GPIO_Port GPIOD
#define LCD1_Pin GPIO_PIN_9
#define LCD1_GPIO_Port GPIOD
#define LCD2_Pin GPIO_PIN_10
#define LCD2_GPIO_Port GPIOD
#define LCD3_Pin GPIO_PIN_11
#define LCD3_GPIO_Port GPIOD
#define LCD4_Pin GPIO_PIN_12
#define LCD4_GPIO_Port GPIOD
#define LCD5_Pin GPIO_PIN_13
#define LCD5_GPIO_Port GPIOD
#define LCD6_Pin GPIO_PIN_14
#define LCD6_GPIO_Port GPIOD
#define LCD7_Pin GPIO_PIN_15
#define LCD7_GPIO_Port GPIOD
#define LCD_R_W_Pin GPIO_PIN_7
#define LCD_R_W_GPIO_Port GPIOC
#define LCD_E_Pin GPIO_PIN_8
#define LCD_E_GPIO_Port GPIOC
#define LCD_RS_Pin GPIO_PIN_12
#define LCD_RS_GPIO_Port GPIOC
#define Debounce_RS_En_Pin GPIO_PIN_5
#define Debounce_RS_En_GPIO_Port GPIOB
#define Rotary_Decoder_Pin GPIO_PIN_6
#define Rotary_Decoder_GPIO_Port GPIOB
#define Rotary_DecoderB7_Pin GPIO_PIN_7
#define Rotary_DecoderB7_GPIO_Port GPIOB
#define PWM_DRS_Pin GPIO_PIN_8
#define PWM_DRS_GPIO_Port GPIOB
#define SDB_Dash_in_Pin GPIO_PIN_9
#define SDB_Dash_in_GPIO_Port GPIOB
#define LR1_Pin GPIO_PIN_0
#define LR1_GPIO_Port GPIOE
#define LR2_Pin GPIO_PIN_1
#define LR2_GPIO_Port GPIOE

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */
#define TRUE 1
#define FALSE 0
/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
