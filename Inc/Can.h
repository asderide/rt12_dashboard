/*
 * Can_ID.h
 *
 *  Created on: 31.05.2018
 *      Author: asder
 */

#ifndef CAN_H_
#define CAN_H_

#include "stm32f4xx_hal.h"

#define Dashboard_ID			600			//CAN ID Dashboard

#define Kl50_ID		        	605			//CAN ID Druckknopf 1
#define Inv_Res_ID				611			//CAN ID Druckknopf 2
#define VDCU_Res_ID				612			//CAN ID Druckknopf 3
#define RTC_ID					620			//CAN ID Echtzeit-Uhr
#define dash_status_ID			640			//CAN ID Dashboard Status
#define PedCal_ID				651			//CAN ID Druckknopf 4
#define SuspCal_ID				650			//CAN ID Druckknopf 5
#define Reserve_ID				652			//CAN ID Druckknopf 6
#define Control_Mode_ID			661			//CAN ID Reglerstufe
#define Rekuperation_ID			662			//CAN ID Rekuperation
#define Slip_Control_ID			663			//CAN ID Traktionskontrolle
#define Torque_Control_ID		664			//CAN ID Torque Vectoring
#define Vehicle_Power_ID		665			//CAN ID Fahrzeugleistung
#define Drive_Setup_ID			666			//CAN ID Setupauswahl
#define SC_Res_ID				670			//CAN ID Shutdown Reset
#define Fan_ID					675			//CAN ID L�fter


//Ankommende Nachrichten Dashboard

//VCU
#define VCU_ID					100			//CAN ID VCU
#define Klemme_50_aktiv_ID  	102			//CAN ID Klemme 50 aktiv
#define VCU_Status_ID			130			//CAN ID VCU Status Nachricht

//VDCU
#define VDCU_Status_ID			150			//CAN ID VDCU - Status Nachrichten
#define VDCU_Electrical_ID		153			//CAN ID VDCU - Elektrische Daten Antriebsstrang
#define VDCU_Wheelspeeds_ID		154			//CAN ID VDCU - Wheelspeeds
#define VDCU_Velocity_ID		155			//CAN ID VDCU - Velocity
#define VDCU_Derating_ID		157			//CAN ID VDCU - Power Control
#define VDCU_Parameter_ID		162			//CAN ID VDCU - Parameter zum Anzeigen im Display
#define VDCU_Drivetrain_ID		164			//CAN ID VDCU - Statusdaten des Antriebstranges
#define VDCU_CTRL_MODE_ID		171			//CAN ID VDCU - Reglerstufe
#define VDCU_RECUP_ID			172			//CAN ID VDCU - Parameter Rekuperation
#define VDCU_SLIP_CTRL_ID		173			//CAN ID VDCU - Parameter Traktionskontrolle
#define VDCU_TORQUE_CTRL_ID		174			//CAN ID VDCU - Parameter Torque Vectoring
#define VDCU_POWER_ID			175			//CAN ID VDCU - Parameter Leistung / Drehmoment
#define VDCU_SETUP_ID			176			//CAN ID VDCU - Parameter Setup

//AMS
#define AMS_voltage_ID			207			//CAN ID Gesamtspannung Akku
#define AMS_temp_ID				208			//CAN ID Temperaturen Akku
#define AMS_current_ID			210			//CAN ID Gesamtstrom Tractive System
#define SoC_Akku_ID				215			//CAN ID State of Charge Akku

//Pedalerie
#define Ped_Throttle_ID			301			//CAN ID Pedalerie Gaspedal
#define Ped_Brake_ID			302			//CAN ID Pedalerie Bremspedal
#define Ped_Stat_ID				310			//CAN ID Pedalerie Status f�r BOTS

//Messbox
#define MU_front_state_ID		401			//CAN ID Messbox Status vorne
#define MU_front_suspension_ID	405			//CAN ID Messbox Federwege vorne
#define MU_front_steering_ID	415			//CAN ID Messbox Lenkwinkel
#define MU_front_brake_ID		420			//CAN ID Messbox Bremsdruck vorne
#define MU_front_temp_ID		440			//CAN ID Messbox Temperaturen vorne

#define MU_rear_state_ID		451			//CAN ID Messbox Status hinten
#define MU_rear_suspension_ID	455			//CAN ID Messbox Federwege hinten
#define MU_rear_tcool_ID		460			//CAN ID Messbox K�hlertemperatur
#define MU_rear_brake_ID		470			//CAN ID Messbox Bremsdruck hinten
#define MU_rear_temp_ID			490			//CAN ID Messbox Temperaturen hinten

//DCDC-Wandler
#define	DCDC_Temp_ID			705			//CAN ID DCDC-Wandler Temperaturen

#define IVT_Voltage_ID			251			//CAN ID Zwischenkreisspannung HV

//typedef CanRxMsgTypeDef;

void receive_CAN_interrupt(int);


#endif /* CAN_ID_H_ */
