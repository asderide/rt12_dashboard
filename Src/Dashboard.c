/*
 * Dashboard.c
 *
 *  Created on: 31.05.2018
 *      Author: asder
 */

#include <Can.h>
#include "Dashboard.h"
//#include "main.h"
#include "stm32f4xx_hal.h"
#include "globvars.h"
#include "Display.h"

uint8_t daten[2];
extern SPI_HandleTypeDef hspi1;

void startBusy()
{
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1, GPIO_PIN_SET);
}

void endBusy()
{
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1, GPIO_PIN_RESET);
}

void displayErrorState(HAL_StatusTypeDef *state)
{
	if(*state == HAL_OK)
	{
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_RESET);
		return;
	}
	else if(*state == HAL_ERROR)
	{
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_SET);
		HAL_Delay(1);
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_RESET);
		HAL_Delay(3);
	}
	else if(*state == HAL_BUSY)
	{
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_SET);//an
		HAL_Delay(1);
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_RESET);//aus
		HAL_Delay(5);
	}
	else if(*state == HAL_TIMEOUT)
	{
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_SET);
		HAL_Delay(1);
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, GPIO_PIN_RESET);
		HAL_Delay(2);
	}
	return;
}

void writeFloatInMessage(CanTxMsgTypeDef *msg, uint32_t value)
{
	msg->DLC = 4;
	msg->Data[0] = (value >>  0) & 0xFF;
	msg->Data[1] = (value >>  8) & 0xFF;
	msg->Data[2] = (value >> 16) & 0xFF;
	msg->Data[3] = (value >> 24) & 0xFF;
}

void sendCanMsg(CAN_HandleTypeDef *hcan1)
{
	startBusy();
	HAL_StatusTypeDef result = HAL_CAN_Transmit(hcan1, 100);
	endBusy();
	displayErrorState(&result);
}

void DisplayError(void)
{

}

void Bestaetigung (void)
{

}

void WriteGPIOExpand (SPI_HandleTypeDef *hspi1, int GPIOAddress, int GPIOvalue)
{
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
	wait(10);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
	startBusy();

	daten[0]=0x00|GPIOAddress&127;
	daten[1]=GPIOvalue;
	HAL_SPI_Transmit(&hspi1, daten, 2, 10);
	endBusy();
}

void WriteLEDDriver (int LEDAddress, int LEDValue)
{
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_SET);		//CS fuer alle SPI Geraete auf High ziehen
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
	wait(10);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_RESET);
	startBusy();
	daten[0]=0x00|LEDAddress&127;
	daten[1]=LEDValue;

	if (HAL_SPI_Transmit(&hspi1, daten, 2, 10) == HAL_OK)
	{
		endBusy();
	}
	else
	{
		int SPI_Error = 1;
	}
}



void Check2330(void)
{
switch(Port2330[1])
{
case 0b01111111: Group = 11; break;
case 0b10111111: Group = 10; break;
case 0b11011111: Group = 9; break;
case 0b11101111: Group = 8; break;
case 0b11110111: Group = 7; break;
case 0b11111011: Group = 6; break;
case 0b11111101: Group = 4; break;
case 0b11111110: Group = 2; break;
}
}

void Check47(void)
{
switch(Port47[1])
{
case 0b10111111: Group = 1; break;
case 0b11011111: Group = 3; break;
case 0b11101111: Group = 5; break;
}
}

void Check916(void)
{
switch(Port916[1])
{
case 0b01111111: DisplayMenuSoC(); break;
case 0b10111111: DisplayMenuControlMode(); break;
case 0b11011111: DisplayMenuRekuperation(); break;
case 0b11110111: DisplayMenuSchlupfkontrolle(); break;
case 0b11111011: DisplayMenuMomentensteuerung(); break;
case 0b11111101: DisplayMenuFahrzeugleistung(); break;
case 0b11111110: DisplayMenuSetupauswahl(); break;
}
}

void Check1421(void)
{

	switch(Port1421[1])
	{
	case 0b11101111: DisplayMenuFahrzeugstatus_1(); break;
	case 0b11110111: DisplayMenuFahrzeugstatus_2(); break;
	case 0b11111011: Menu = 10; break;
	case 0b11111101: Menu = 11; break;
	case 0b11111110: Menu = 12; break;
	}
}

void LEDDriverInit (void)
{

	WriteLEDDriver(0x02, 0x07);
	WriteLEDDriver(0x09, 0x00);
	WriteLEDDriver(0x0A, 0x00);
	WriteLEDDriver(0x0B, 0x00);
	WriteLEDDriver(0x0C, 0x00);
	WriteLEDDriver(0x0D, 0x00);
	WriteLEDDriver(0x0E, 0x00);
	WriteLEDDriver(0x0F, 0x00);

	WriteLEDDriver(0x04, 0x01);



//	WriteLEDDriver(0x04, 0b00000001);			//Configuration Register kein Pin Change, Global Current und normal operation
//	WriteLEDDriver(0x02, 0b00000111);			//Global Current auf 50% setzen
//
//	WriteLEDDriver(0x09, 0b0);					// Alle Pins als LED Output definieren
//	WriteLEDDriver(0x0A, 0b0);
//	WriteLEDDriver(0x0B, 0b0);
//	WriteLEDDriver(0x0C, 0b0);
//	WriteLEDDriver(0x0E, 0b0);
//	WriteLEDDriver(0x0D, 0b0);
//	WriteLEDDriver(0x0F, 0b0);
//
//	WriteLEDDriver(0x07, 0b01);
//
//	WriteLEDDriver(0x09, 0b0);
//	WriteLEDDriver(0x0A, 0b0);
//	WriteLEDDriver(0x0B, 0b0);
//	WriteLEDDriver(0x0C, 0b0);
//	WriteLEDDriver(0x0E, 0b0);
//	WriteLEDDriver(0x0D, 0b0);
//	WriteLEDDriver(0x0F, 0b0);
}

void GPIOExpandInit (void)
{
	WriteGPIOExpand(&hspi1, 0x04, 0b0);
	WriteGPIOExpand(&hspi1, 0x06, 0b0);
	WriteGPIOExpand(&hspi1, 0x09, 0b10101010);
	WriteGPIOExpand(&hspi1, 0x0A, 0b10101010);
	WriteGPIOExpand(&hspi1, 0x0B, 0b10101010);
	WriteGPIOExpand(&hspi1, 0x0C, 0b10101010);
	WriteGPIOExpand(&hspi1, 0x0D, 0b10101010);
	WriteGPIOExpand(&hspi1, 0x0E, 0b10101010);
	WriteGPIOExpand(&hspi1, 0x0F, 0b10101010);
}

uint16_t getParameterValue(uint8_t SwitchNr)
{
	uint16_t 	value = status.MinMax[Nr_Min][SwitchNr] + Rotary;	// Neuer Parameterwert: Minimum + (Drehpoti-ADC-Wert /  Aufl�sung)

	if(value < status.MinMax[Nr_Min][SwitchNr])
		value = status.MinMax[Nr_Min][SwitchNr];
	else if(value > status.MinMax[Nr_Max][SwitchNr])
		value = status.MinMax[Nr_Max][SwitchNr];

//ToDo: �berpr�fung, ob Dashboard �berhaupt Parameter ver�ndern darf, fehlt in der ersten If-Abfrage! Hat aber trotzdem keinen Einfluss auf die Funktionsweise, kann also so belassen werden, auch wenn es nicht sch�n ist :P
	if(RT10_input.translation.Para_OK.active && !RT10_input.translation.Para_OK.exists && VDCU_Status.translation.calibration_state == 0 && !HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_5))		// Nur aktiv, wenn Knopf gedr�ckt wurde, aber nicht mehr gedr�ckt ist und von der VDCU die Erlaubnis zum �ndern der Parameterwerte vorhanden ist
	{
		for(uint8_t i = 0; i < 20; i++)
		{
			status.MinMax[Nr_Dashboard][i] 			= status.MinMax[Nr_VDCU][i];
		}
		status.failure.value 						= status.MinMax[Nr_VDCU][SwitchNr];
		status.MinMax[Nr_Dashboard][SwitchNr] 		= value;
		RT10_input.translation.Para_OK.active 		= FALSE;
		RT10_input.translation.Para_OK.exists		= TRUE;
	}

	else if(RT10_input.translation.Para_OK.active == 1 || VDCU_Status.translation.calibration_mode != 1 || VDCU_Status.translation.calibration_state == 1)		// Wenn Knopf gedr�ckt wurde, die VDCU aber keine Erlaubnis zum Ver�ndern erteilt hat
	{
		RT10_input.translation.Para_OK.active 		= FALSE;
	}

	if(VDCU_Status.translation.calibration_state == 2)
	{
		status.MinMax[Nr_Dashboard][SwitchNr] 		= status.failure.value;
		RT10_input.translation.Para_OK.active 		= FALSE;
		RT10_input.translation.Para_OK.exists		= FALSE;
	}

	return(value);
}

int ReadGPIOExpand ()
{
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_SET);		//CS fuer alle SPI Geraete auf High ziehen
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
	wait(10);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
	startBusy();
	daten[1]=0b11010111;		//Pins 23-30 auslesen
	daten[0]=0b11010111;
	HAL_SPI_Transmit(&hspi1, daten, 2, 10);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
	wait(10);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
	HAL_SPI_Receive(&hspi1, Port2330, 2, 10);

//	Port2330[0] = Port2330Raw >> 8;     // high byte
//	Port2330[1] = Port2330Raw & 0x00FF; // low byte

	if(Port2330[1]==0b11111111)
	{
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
	wait(10);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
	startBusy();
	daten[0]=0b01000000;		//Pins 4-7 auslesen, 4 ist NC
	daten[1]=0;
	HAL_SPI_Transmit(&hspi1, daten, 2, 10);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
	wait(10);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
	HAL_SPI_Receive(&hspi1, Port47, 2, 10);

//	Port47[0] = Port47Raw >> 8;     // high byte
//	Port47[1] = Port47Raw & 0x00FF; // low byte

	if(Port47[1] == 0b11110000)
	{
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
	wait(10);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
	startBusy();
	daten[0]=0b00110110;		//Pin 22 auslesen
	daten[1]=0;
	HAL_SPI_Transmit(&hspi1, daten, 2, 10);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
	wait(10);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
	HAL_SPI_Receive(&hspi1, Port22, 2, 10);


//	Port22[0] = Port22Raw >> 8;     // high byte
//	Port22[1] = Port22Raw & 0x00FF; // low byte
	if(Port22[1] == 0b10000000)
	{
		DisplayError();
	}
	else
	{
		Group = 12;
	}
	}
	else
	{
		Check47();
	}
	}
	else
	{
		Check2330();
	}

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
	wait(10);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
	startBusy();
	daten[0]=0b01001001;		//Pins 9-16 auslesen
	daten[1]=0;
	HAL_SPI_Transmit(&hspi1, daten, 2, 10);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
	wait(10);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
	HAL_SPI_Receive(&hspi1, Port916, 2, 10);

//	Port916[0] = Port916Raw >> 8;     // high byte
//	Port916[1] = Port916Raw & 0x00FF; // low byte

	if(Port916[1]==0b11111111)
	{
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
	wait(10);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
	startBusy();
	daten[0]=0b01001110;		//Pins 14-21 auslesen
	daten[1]=0;
	HAL_SPI_Transmit(&hspi1, daten, 2, 10);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
	wait(10);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
	HAL_SPI_Receive(&hspi1, Port1421, 2, 10);

//	Port1421[0] = Port1421Raw >> 8;     // high byte
//	Port1421[1] = Port1421Raw & 0x00FF; // low byte

	if(Port1421[1]==0b11111111)
	{
		DisplayError();
	}
	else
	{
		Check1421();
	}
	}
	else
	{
		Check916();
	}

//	SetMenu();
//	SetGroup();

	endBusy();
}
//void SetMenu(void)
//{
//
//}
//
//void SetGroup(void)
//{
//
//}

//void update_LEDs (void)
//{
//	if(RT10_input.translation.IMD.active == TRUE)
//			switch_LED(LED_IMD_green, LED_OFF);
//		else
//			switch_LED(LED_IMD_green, LED_ON);
//
//	if(RT10_input.translation.BMS.active == TRUE)
//			switch_LED(LED_BMS_green, LED_OFF);
//		else
//			switch_LED(LED_BMS_green, LED_ON);
//
//	if(RT10_input.translation.BSPD.active == TRUE)
//			switch_LED(LED_BSPD_green, LED_OFF);
//		else
//			switch_LED(LED_BSPD_green, LED_ON);
//
//	if(RT10_input.translation.IS.active == TRUE)
//			switch_LED(LED_SC_red, LED_ON),
//			switch_LED(LED_SC_green, LED_OFF);
//		else
//			switch_LED(LED_SC_red, LED_OFF),
//			switch_LED(LED_SC_green, LED_ON);
//
//	if(RT10_input.translation.SBD.active == TRUE)
//			switch_LED(LED_TK_red, LED_ON),
//			switch_LED(LED_TK_green, LED_OFF);
//		else
//			switch_LED(LED_TK_red, LED_OFF),
//			switch_LED(LED_TK_green, LED_ON);
//
//	if(RT10_input.translation.SBl.active == TRUE)
//			switch_LED(LED_TV_red, LED_ON),
//			switch_LED(LED_TV_green, LED_OFF);
//		else
//			switch_LED(LED_TV_red, LED_OFF),
//			switch_LED(LED_TV_green, LED_ON);
//
//	if(RT10_input.translation.SBr.active == TRUE)
//			switch_LED(LED_REC_red, LED_ON),
//			switch_LED(LED_REC_green, LED_OFF);
//		else
//			switch_LED(LED_REC_red, LED_OFF),
//			switch_LED(LED_REC_green, LED_ON);
//
//	if(RT10_input.translation.BOTS.active == TRUE)
//			switch_LED(LED_TEMP_red, LED_ON),
//			switch_LED(LED_TEMP_green, LED_OFF);
//		else
//			switch_LED(LED_TEMP_red, LED_OFF),
//			switch_LED(LED_TEMP_green, LED_ON);
//
//	if(RT10_input.translation.BMS_Temp.active == TRUE)
//			switch_LED(LED_SOC_red, LED_ON),
//			switch_LED(LED_SOC_green, LED_OFF);
//		else
//			switch_LED(LED_SOC_red, LED_OFF),
//			switch_LED(LED_SOC_green, LED_ON);
//
//	if(RT10_input.translation.BMS_UV.active == TRUE)
//			switch_LED(LED_SENS_red, LED_ON),
//			switch_LED(LED_SENS_green, LED_OFF);
//		else
//			switch_LED(LED_SENS_red, LED_OFF),
//			switch_LED(LED_SENS_green, LED_ON);
//
//	if(RT10_input.translation.BMS_OV.active == TRUE)
//			switch_LED(LED_FANred, LED_ON),
//			switch_LED(LED_FANgreen, LED_OFF);
//		else
//			switch_LED(LED_FANred, LED_OFF),
//			switch_LED(LED_FANgreen, LED_ON);
//
//	if(RT10_input.translation.DRS.active == TRUE)
//			switch_LED(LED_DRS_red, LED_ON),
//			switch_LED(LED_DRS_green, LED_OFF);
//		else
//			switch_LED(LED_DRS_red, LED_OFF),
//			switch_LED(LED_DRS_green, LED_ON);
//
//	if(RT10_input.translation.Kl50.alive)
//			switch_LED(LED_Kl50, LED_ON);
//		else
//			switch_LED(LED_Kl50, LED_OFF);
//
//	if(VCU_Status.translation.INVl_power && VCU_Status.translation.INVr_power)
//			switch_LED(LED_Inv_Res, LED_ON);
//		else
//			switch_LED(LED_Inv_Res, LED_OFF);
//
//	if(RT10_input.translation.Module_VDCU.alive)
//			switch_LED(LED_VDCU_Res, LED_ON);
//		else
//			switch_LED(LED_VDCU_Res, LED_OFF);
//
//	if(!RT10_input.translation.preCharge.state && RT10_input.translation.PreCharge.value_f > 0.9)
//			switch_LED(LED_SC_Res, LED_ON);
//	else if(!RT10_input.translation.preCharge.state && RT10_input.translation.PreCharge.value_f < 0.9)
//			switch_LED(LED_SC_Res, LED_OFF);
//
//if(RT10_input.translation.preCharge.state == TRUE)
//{
//	if(timer_preCharge <= 0)
//		switch_LED(LED_SC_Res, LED_ON),
//		timer_preCharge = 200;
//	else if(timer_preCharge > 0 && timer_preCharge < 100)
//		switch_LED(LED_SC_Res, LED_OFF);
//}
//
//	if(Pedalerie.translation.br_gas_inplau || Pedalerie.translation.gas_inplau)
//			switch_LED(LED_FAN_button, LED_ON);
//		else
//			switch_LED(LED_FAN_button, LED_OFF);
//
//	if(VCU_Status.translation.Luefter_links_power && VCU_Status.translation.Luefter_rechts_power)
//			switch_LED(LED_Reserve, LED_ON);
//		else
//			switch_LED(LED_Reserve, LED_OFF);
//
//}
//
//void switch_LED(int LED, int Status)
//{
//	switch(LED)
//		{
//			case (LED_SC_green):		WriteLEDDriver(LED_SC_green,		Status); 	break;
//			case (LED_SC_red):			WriteLEDDriver(LED_SC_red,			Status); 	break;
//			case (LED_TK_green):		WriteLEDDriver(LED_TK_green,		Status); 	break;
//			case (LED_TK_red):			WriteLEDDriver(LED_TK_red,			Status); 	break;
//			case (LED_TV_green):		WriteLEDDriver(LED_TV_green,		Status); 	break;
//			case (LED_TV_red):			WriteLEDDriver(LED_TV_red,			Status); 	break;
//			case (LED_REC_green):		WriteLEDDriver(LED_REC_green,		Status); 	break;
//			case (LED_REC_red):			WriteLEDDriver(LED_REC_red,			Status); 	break;
//			case (LED_TEMP_green):		WriteLEDDriver(LED_TEMP_green,		Status);	break;
//			case (LED_TEMP_red):		WriteLEDDriver(LED_TEMP_red,		Status); 	break;
//			case (LED_SOC_green):		WriteLEDDriver(LED_SOC_green,		Status); 	break;
//			case (LED_SOC_red):			WriteLEDDriver(LED_SOC_red,			Status); 	break;
//			case (LED_SENS_green):		WriteLEDDriver(LED_SENS_green,		Status); 	break;
//			case (LED_SENS_red):		WriteLEDDriver(LED_SENS_red,		Status);	break;
//			case (LED_FANgreen):		WriteLEDDriver(LED_FANgreen,		Status); 	break;
//			case (LED_FANred):			WriteLEDDriver(LED_FANred,			Status); 	break;
//			case (LED_Inv_Res):			WriteLEDDriver(LED_Inv_Res,			Status); 	break;
//			case (LED_VDCU_Res):		WriteLEDDriver(LED_VDCU_Res,		Status); 	break;
//			case (LED_SC_Res):			WriteLEDDriver(LED_SC_Res,			Status); 	break;
//			case (LED_FAN_button):		WriteLEDDriver(LED_FAN_button,		Status); 	break;
//
//
//			default:																			break;
//			}
//}
