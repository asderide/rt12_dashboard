/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"

/* USER CODE BEGIN Includes */
#include "Can.h"
#include "Dashboard.h"
#include "globvars.h"
#include "Display.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
CAN_HandleTypeDef hcan1;

SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
extern Menu_position = 0;
extern Rotary = 0;
extern Group = 0;
extern Menu = 0;
uint8_t daten[2];



/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_CAN1_Init(void);
static void MX_TIM4_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM2_Init(void);
static void MX_SPI1_Init(void);

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);
                                

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void HAL_CAN_RxCpltCallback(CAN_HandleTypeDef* hcan);
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_CAN1_Init();
  MX_TIM4_Init();
  MX_TIM3_Init();
  MX_TIM2_Init();
  MX_SPI1_Init();

  /* USER CODE BEGIN 2 */
  __HAL_CAN_ENABLE_IT(&hcan1, CAN_IT_FMP0);
    TxMessage.IDE = CAN_ID_STD;
    TxMessage.RTR = CAN_RTR_DATA;

    myFilter.FilterNumber = 0;
    myFilter.FilterMode = CAN_FILTERMODE_IDMASK;
    myFilter.FilterScale = CAN_FILTERSCALE_32BIT;
    myFilter.FilterIdHigh = 0x0000;
    myFilter.FilterIdLow = 0x0000;
    myFilter.FilterMaskIdHigh = 0x0000;
    myFilter.FilterMaskIdLow = 0x0000;
    myFilter.FilterFIFOAssignment = 0;
    myFilter.FilterActivation = ENABLE;

    HAL_CAN_ConfigFilter(&hcan1,&myFilter);

    Dashboard_Status.translation.Kl50_active=0;
    Dashboard_Status.translation.Inv_Res_active=0;
    Dashboard_Status.translation.VDCU_Res_active=0;
    Dashboard_Status.translation.Fan_and_Pump_active=0;
    Dashboard_Status.translation.IS_active=0;
    Dashboard_Status.translation.BMS_Temp_active=0;
    Dashboard_Status.translation.BMS_UV_active=0;
    Dashboard_Status.translation.BMS_OV_active=0;
    Dashboard_Status.translation.Para_Change_active=0;
    Dashboard_Status.translation.Reset_Distance_active=0;
    Dashboard_Status.translation.SC_active=0;
    Update = 0;


    DisplayInit();

//    setPositionDisplay(LINE1);
//    writeStringDisplay("1");
//    setPositionDisplay(LINE2);
//    writeStringDisplay("2");
//    setPositionDisplay(LINE3);
//    writeStringDisplay("3");
//    setPositionDisplay(LINE4);
//    writeStringDisplay("4");
    setPositionDisplay(LINE2);
    writeStringDisplay("maybe today");


      HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_3);
      HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_SET);		//CS fuer alle SPI Geraete auf High ziehen
      HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
      HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);

      HAL_TIM_Base_Start_IT(&htim3);
      HAL_TIM_Base_Start_IT(&htim2);
//      LEDDriverInit();
      GPIOExpandInit();


      CAN_Received = 0;
      Fan_pushed = 0;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
//	  setPositionDisplay(LINE3);
//	  writeStringDisplay("DRS auf:");

	  HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_0);

	  hcan1.pRxMsg = &msg;

//	  daten[1]=0b11010111;		//Pins 23-30 auslesen
//	  	daten[0]=0b11010111;
//	  	HAL_SPI_Transmit(&hspi1, daten, 2, 10);
//	  	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_SET);
//	  	wait(10);
//	  	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);
//	  	HAL_SPI_Receive(&hspi1, Port2330, 2, 10);

	  HAL_CAN_Receive_IT(&hcan1, CAN_FILTER_FIFO0);
//	  WriteLEDDriver(0x07, 0b0);

	  	  Rotary = TIM4->CNT;

	  	  if(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_0)) 		//LR Knoepfe auslesen
	  	  	  {
	  		  htim4.Instance->CCR3 = 3000;				//DRS PWM Duty Cycle anpassen
	  	  	  }
	  	  else
	  	  {
	  		  htim4.Instance->CCR3 = 4000;
	  	  }

	  	  if(!HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_1))
	  	 	  {
	  		LR2_pushed = 1;
	  	 	  }
	  	  else
	  	  {
	  		  if(LR2_pushed==1)
	  		  {
	  			Dashboard_Status.translation.TV++;
	  		  }
	  	  }

	  	  if(!HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_2))
	  	 	  {
	  		  LR3_pushed = 1;
	  	 	  }
	  	  else
	  	  {
	  		  if(LR3_pushed == 1)
	  		  {
	  			  Dashboard_Status.translation.TV--;
	  		  }
	  	  }

	  	  if(!HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_3))
	  	 	  {
	  		  	  LR4_pushed = 1;
	  	 	  }
	  	  else
	  	  {
	  		  if(LR4_pushed == 1)
	  		  {
	  			  Dashboard_Status.translation.REC++;
	  		  }
	  	  }

	  	  if(!HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_4))
	  	 	  {
	  		  	  LR5_pushed = 1;
	  	 	  }
	  	  else
	  	  {
	  		  if(LR5_pushed == 1)
	  		  {
	  			  Dashboard_Status.translation.REC--;
	  		  }
	  	  }



	  	  if(!HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_5)) 		//Bestaetigungsknopf auslesen
	  	 	  {
	  		  Bestaetigung();
	  	 	  }


	  	  if(!HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_6)) 		//Startknopf auslesen
	  	 	  {
//	  		htim4.Instance->CCR3 = 4000;
//	  		writeCharDisplay("+");
//	  		sendDisplayDigit(90);
	  		Dashboard_Status.translation.Kl50_active = 1;
	  		  Start_pushed = 1;
	  	 	  }
	  	  else
	  	  {
	  		Dashboard_Status.translation.Kl50_active = 0;
//	  		  if(Start_pushed == 1)
//	  		  {
//	  			Dashboard_Status.translation.Kl50_active = 1;
//	  			Start_pushed = 0;
//	  		  }

	  	  }


	  	  if(!HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_7))		//DK1 Inv Reset
	  	  	  {
//	  		htim4.Instance->CCR3 = 2000;
//	  		writeCharDisplay("-");
//	  		sendDisplayDigit(90);
	  		Dashboard_Status.translation.Inv_Res_active = 1;
	  		  Inv_res_pushed = 1;
	  	  	  }
	  	  else
	  	  {
	  		Dashboard_Status.translation.Inv_Res_active = 0;
//	  		  if(Inv_res_pushed == 1)
//	  		  {
//	  			  Dashboard_Status.translation.Inv_Res_active = 1;
//	  			  Inv_res_pushed = 0;
//	  		  }
	  	  }

	  	  if(!HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_2))		//DK2 VDCU Reset
	  	  	  {
//		  		htim4.Instance->CCR3 = 2500;
//		  		writeCharDisplay("-");
//		  		sendDisplayDigit(45);
	  		Dashboard_Status.translation.VDCU_Res_active = 1;
	  		  VDCU_Res_pushed = 1;
	  	  	  }
	  	  else
	  	  {
	  		Dashboard_Status.translation.VDCU_Res_active = 0;
//	  		  if(VDCU_Res_pushed == 1)
//	  		  {
//	  			  Dashboard_Status.translation.VDCU_Res_active = 1;
//	  			  VDCU_Res_pushed = 0;
//	  		  }
	  	  }

	  	  if(!HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_1))		//DK3 SC Reset
	  	  	  {
//		  		htim4.Instance->CCR3 = 3500;
//		  		writeCharDisplay("+");
//		  		sendDisplayDigit(45);
	  		Dashboard_Status.translation.Fan_and_Pump_active = 1;
	  		  SC_Res_pushed = 1;
	  	  	  }
	  	  else
	  	  {
	  		Dashboard_Status.translation.Fan_and_Pump_active = 0;
//	  		  if(SC_Res_pushed == 1)
//	  		  {
//	  			  Dashboard_Status.translation.SC_Reset = 1;
//	  			  SC_Res_pushed = 0;
//	  		  }
	  	  }

	  	  if(!HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_0))		//DK4 FAN on/off
	  	  	  {
	  		Dashboard_Status.translation.SC_Reset = 1;
	  		  Fan_pushed = 1;
	  	  	  }
	  	  else
	  	  {
	  		Dashboard_Status.translation.SC_Reset = 0;
//	  		  if(Fan_pushed == 1)
//	  		  {
//	  			Dashboard_Status.translation.Fan_and_Pump_active = 1;
//	  			Fan_pushed = 0;
//	  		  }
	  	  }
//	  	  if (CAN_Received==1)
//	  	  {
//	  		receive_CAN_interrupt(CAN_Receive_ID);
//	  			  	  TxMessage.StdId = 601;
//	  			  	  TxMessage.Data[0] = 0;
//	  			  	  TxMessage.DLC = 1;
//	  			      TxMessage.IDE = CAN_ID_STD;
//	  			      TxMessage.RTR = CAN_RTR_DATA;
//
//	  			      hcan1.pTxMsg = &TxMessage;
//	  			  	  HAL_CAN_Transmit(&hcan1, 100);
//
//	  			  	  CAN_Received = 0;
//	  	  }

//	  	  if(CAN_Received_1234 == 1)
//	  	  {
//	  			  			  	  TxMessage.StdId = 601;
//	  			  			  	  TxMessage.Data[0] = 0;
//	  			  			  	  TxMessage.DLC = 1;
//	  			  			      TxMessage.IDE = CAN_ID_STD;
//	  			  			      TxMessage.RTR = CAN_RTR_DATA;
//
//	  			  			      hcan1.pTxMsg = &TxMessage;
//	  			  			  	  HAL_CAN_Transmit(&hcan1, 100);
//
//	  			  			  	  CAN_Received = 0;
//	  	  }

		  if(HAL_CAN_Receive_IT(&hcan1, CAN_FILTER_FIFO0) != HAL_OK)
		  {

		  }

		  if(Dashboard_Status_Send_1 == 1 && Dashboard_Status_Send_2 == 1)
		  {
			  startBusy();
			  Dashboard_Status_Send_1 = 0;
			  Dashboard_Status_Send_2 = 0;
			  TxMessage.StdId = 640;
			  TxMessage.Data[0] = Dashboard_Status.translation.Kl50_active + Dashboard_Status.translation.Inv_Res_active*2 + Dashboard_Status.translation.VDCU_Res_active*4 + Dashboard_Status.translation.Fan_and_Pump_active*8 + Dashboard_Status.translation.IS_active*16 + Dashboard_Status.translation.BMS_Temp_active*32 + Dashboard_Status.translation.BMS_UV_active*64 + Dashboard_Status.translation.BMS_OV_active*128;
			  TxMessage.Data[1] = Dashboard_Status.translation.Para_Change_active + Dashboard_Status.translation.Reset_Distance_active*2 + Dashboard_Status.translation.SC_active*4 + Dashboard_Status.translation.SC_Reset*16;
			  TxMessage.DLC = 2;
			  TxMessage.IDE = CAN_ID_STD;
			  TxMessage.RTR = CAN_RTR_DATA;

			  hcan1.pTxMsg = &TxMessage;
			  HAL_CAN_Transmit(&hcan1, 20);

			  Dashboard_Status_Send_1 = 0;
			  Dashboard_Status_Send_2 = 0;
//			  Dashboard_Status.translation.Kl50_active=0;
//			  Dashboard_Status.translation.Inv_Res_active=0;
//			  Dashboard_Status.translation.VDCU_Res_active=0;
//			  Dashboard_Status.translation.Fan_and_Pump_active=0;
//			  Dashboard_Status.translation.IS_active=0;
//			  Dashboard_Status.translation.BMS_Temp_active=0;
//			  Dashboard_Status.translation.BMS_UV_active=0;
//			  Dashboard_Status.translation.BMS_OV_active=0;
//			  Dashboard_Status.translation.Para_Change_active=0;
//			  Dashboard_Status.translation.Reset_Distance_active=0;
//			  Dashboard_Status.translation.SC_active=0;
			  Dashboard_Status_Send_1 = 0;
			  Dashboard_Status_Send_2 = 0;
			  endBusy();
		  }

		  if(Update == 1)
		  {
			  Update = 0;
			  DisplayMenuTractiveSystem();
		  }
//
//	  	  ReadGPIOExpand();
//
//	  	setPositionDisplay(LINE1);
//	  	sendDisplayDigit(Dashboard_Status.translation.Kl50_active);
//	  	sendDisplayDigit(Dashboard_Status.translation.Inv_Res_active);
//	  	sendDisplayDigit(Dashboard_Status.translation.VDCU_Res_active);
//	  	sendDisplayDigit(Dashboard_Status.translation.SC_Reset);
//	  	sendDisplayDigit(Dashboard_Status.translation.Fan_and_Pump_active);
//	  	sendDisplayDigit(Port2330[0]);
//	  	sendDisplayDigit(Port2330[1]);

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* CAN1 init function */
static void MX_CAN1_Init(void)
{

  hcan1.Instance = CAN1;
  hcan1.Init.Prescaler = 3;
  hcan1.Init.Mode = CAN_MODE_NORMAL;
  hcan1.Init.SJW = CAN_SJW_1TQ;
  hcan1.Init.BS1 = CAN_BS1_11TQ;
  hcan1.Init.BS2 = CAN_BS2_2TQ;
  hcan1.Init.TTCM = DISABLE;
  hcan1.Init.ABOM = DISABLE;
  hcan1.Init.AWUM = DISABLE;
  hcan1.Init.NART = DISABLE;
  hcan1.Init.RFLM = DISABLE;
  hcan1.Init.TXFP = DISABLE;
  if (HAL_CAN_Init(&hcan1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* SPI1 init function */
static void MX_SPI1_Init(void)
{

  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM2 init function */
static void MX_TIM2_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 16000;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 100;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM3 init function */
static void MX_TIM3_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 16000;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 10;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM4 init function */
static void MX_TIM4_Init(void)
{

  TIM_Encoder_InitTypeDef sConfig;
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;

  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 47;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 19999;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_PWM_Init(&htim4) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfig.EncoderMode = TIM_ENCODERMODE_TI12;
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC1Filter = 5;
  sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC2Filter = 5;
  if (HAL_TIM_Encoder_Init(&htim4, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 50;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  HAL_TIM_MspPostInit(&htim4);

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
        * Free pins are configured automatically as Analog (this feature is enabled through 
        * the Code Generation settings)
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, LED2_Pin|LED1_Pin|LCD_R_W_Pin|LCD_E_Pin 
                          |LCD_RS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, Debounce_DK_en_Pin|CS_LED_Pin|CS_GPIO_Pin|CS_RTC_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, RTC_Time_Set_Pin|Debounce_RS_En_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, LCD0_Pin|LCD1_Pin|LCD2_Pin|LCD3_Pin 
                          |LCD4_Pin|LCD5_Pin|LCD6_Pin|LCD7_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : LR3_Pin LR4_Pin LR5_Pin Button_best_Pin 
                           Button_Start_Pin DK1_Pin IMD_Pin BMS_Pin 
                           BSPD_Pin Debounce_DK_int_Pin LR1_Pin LR2_Pin */
  GPIO_InitStruct.Pin = LR3_Pin|LR4_Pin|LR5_Pin|Button_best_Pin 
                          |Button_Start_Pin|DK1_Pin|IMD_Pin|BMS_Pin 
                          |BSPD_Pin|Debounce_DK_int_Pin|LR1_Pin|LR2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : Debounce_RS_Int_Pin */
  GPIO_InitStruct.Pin = Debounce_RS_Int_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(Debounce_RS_Int_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LED2_Pin LED1_Pin LCD_R_W_Pin LCD_E_Pin 
                           LCD_RS_Pin */
  GPIO_InitStruct.Pin = LED2_Pin|LED1_Pin|LCD_R_W_Pin|LCD_E_Pin 
                          |LCD_RS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PC2 PC3 PC4 PC5 
                           PC6 PC9 PC10 PC11 */
  GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5 
                          |GPIO_PIN_6|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : Debounce_DK_en_Pin CS_LED_Pin CS_GPIO_Pin CS_RTC_Pin */
  GPIO_InitStruct.Pin = Debounce_DK_en_Pin|CS_LED_Pin|CS_GPIO_Pin|CS_RTC_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA1 PA8 PA9 PA10 
                           PA11 PA12 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10 
                          |GPIO_PIN_11|GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : DK4_Pin DK3_Pin DK2_Pin RTC_CLK_out_Pin 
                           RTC_Int_Pin SDB_Dash_in_Pin */
  GPIO_InitStruct.Pin = DK4_Pin|DK3_Pin|DK2_Pin|RTC_CLK_out_Pin 
                          |RTC_Int_Pin|SDB_Dash_in_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PE8 PE9 PE10 PE15 */
  GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : PB10 PB11 PB15 */
  GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : RTC_Time_Set_Pin Debounce_RS_En_Pin */
  GPIO_InitStruct.Pin = RTC_Time_Set_Pin|Debounce_RS_En_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : LCD0_Pin LCD1_Pin LCD2_Pin LCD3_Pin 
                           LCD4_Pin LCD5_Pin LCD6_Pin LCD7_Pin */
  GPIO_InitStruct.Pin = LCD0_Pin|LCD1_Pin|LCD2_Pin|LCD3_Pin 
                          |LCD4_Pin|LCD5_Pin|LCD6_Pin|LCD7_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : PD2 PD3 PD4 PD5 
                           PD6 PD7 */
  GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5 
                          |GPIO_PIN_6|GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
//void HAL_CAN_RxCpltCallback(CAN_HandleTypeDef* hcan) {
//CAN_Received=1;
//
//    __HAL_CAN_ENABLE_IT(hcan, CAN_IT_FMP0); // hier mal fest FMPIE0 wieder freigeben, damit weitere CAN-Botschaften (CAN RX ISR ausgel�st werden)
//}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
