/*
 * Can.c
 *
 *  Created on: 31.05.2018
 *      Author: asder
 */

#include "Can.h"
#include "stm32f4xx_hal_can.h"
#include "globvars.h"
#include "main.h"
#include "Dashboard.h"


CAN_HandleTypeDef hcan1;

//void define_MObs(void)
//{
//CanRxMsgTypeDef MOb_VCU =												// Message_Object f�r VCU-Nachrichten
//{
//		CanRxMsgTypeDef.IDE 	= 0b11100011001,
//		CanRxMsgTypeDef.StdId		= 0b00000000000
//};
//
//CanRxMsgTypeDef MOb_VDCU_Status =										// Message_Object f�r VDCU-Nachrichten - Elektrisch
//{
//		IDE 	= 0xFFFFFFFF,
//		CanRxMsgTypeDef.StdId		= VDCU_Status_ID
//};
//
//CanRxMsgTypeDef MOb_VDCU_Electrical =									// Message_Object f�r VDCU-Nachrichten - Elektrisch
//{
//		IDE 	= 0b11111111100,
//		CanRxMsgTypeDef.StdId		= 0b00010011000
//};
//
//CanRxMsgTypeDef MOb_VDCU_Velocity =
//{
//		IDE	= 0b11111111001,
//		CanRxMsgTypeDef.StdId		= 0b00010011001
//};
//
//CanRxMsgTypeDef MOb_VDCU_Parameter =									// Message_Object f�r VDCU-Nachrichten Parameter
//{
//		IDE 	= 0b11111100000,
//		CanRxMsgTypeDef.StdId		= 0b00010100000
//};
//
//CanRxMsgTypeDef MOb_AMS_Voltage =										// Message_Object f�r VDCU_Parameter - CAN-Nachricht
//{
//		IDE 	= 0xFFFFFFFF,
//		CanRxMsgTypeDef.StdId		= AMS_voltage_ID
//};
//
//CanRxMsgTypeDef MOb_AMS_Temp =
//{
//		IDE	= 0xFFFFFFFF,
//		CanRxMsgTypeDef.StdId		= AMS_temp_ID
//};
//
//CanRxMsgTypeDef MOb_Pedal_Unit =										// Message_Object f�r VDCU_Drivetrain - CAN-Nachricht
//{
//		IDE 	= 0b11111100000,
//		CanRxMsgTypeDef.StdId		= 0b00100100000
//};
//
//CanRxMsgTypeDef MOb_Meas_Unit_Front =									// Message_Object f�r VDCU_CTRL_MODE - CAN-Nachricht
//{
//		IDE 	= 0b11111000000,
//		CanRxMsgTypeDef.StdId		= 0b00110000000
//};
//
//CanRxMsgTypeDef MOb_Meas_Unit_Rear =									// Message_Object f�r VDCU_RECUP - CAN-Nachricht
//{
//		IDE 	= 0b11111000000,
//		CanRxMsgTypeDef.StdId		= 0b00111000000
//};
//
//CanRxMsgTypeDef MOb_DCDC =											// Message_Object f�r VDCU_SLIP_CTRL - CAN-Nachricht
//{
//		IDE 	= 0xFFFFFFFF,
//		CanRxMsgTypeDef.StdId		= DCDC_Temp_ID
//};
//
//CAN_enableMOB(MOb_Nr_VCU, 					RECEIVE_DATA, 	MOb_VCU);
//CAN_enableMOB(MOb_Nr_VDCU_Status,			RECEIVE_DATA,	MOb_VDCU_Status);
//CAN_enableMOB(MOb_Nr_VDCU_Electrical, 		RECEIVE_DATA, 	MOb_VDCU_Electrical);
//CAN_enableMOB(MOb_Nr_VDCU_Velocity,			RECEIVE_DATA,	MOb_VDCU_Velocity);
//CAN_enableMOB(MOb_Nr_VDCU_Parameter, 		RECEIVE_DATA, 	MOb_VDCU_Parameter);
//CAN_enableMOB(MOb_Nr_AMS_Voltage,			RECEIVE_DATA, 	MOb_AMS_Voltage);
//CAN_enableMOB(MOb_Nr_AMS_Temp,				RECEIVE_DATA,	MOb_AMS_Temp);
//CAN_enableMOB(MOb_Nr_Pedal_Unit, 			RECEIVE_DATA, 	MOb_Pedal_Unit);
//CAN_enableMOB(MOb_Nr_Meas_Unit_Front, 		RECEIVE_DATA, 	MOb_Meas_Unit_Front);
//CAN_enableMOB(MOb_Nr_Meas_Unit_Rear, 		RECEIVE_DATA, 	MOb_Meas_Unit_Rear);
//CAN_enableMOB(MOb_Nr_DCDC, 					RECEIVE_DATA, 	MOb_DCDC);
//}

	// Zusammenfassung der Spannungs- und Stromwerte von beiden Invertern f�r die SoC- Berechnung
//	RT10_input.translation.current.value 	= abs((VDCU_Electrical.translation.current_inv_left-1023) - (VDCU_Electrical.translation.current_inv_right-1023));
//	RT10_input.translation.voltage.value 	= ROUND((VDCU_Electrical.translation.voltage_inv_left + VDCU_Electrical.translation.voltage_inv_right)/2);

