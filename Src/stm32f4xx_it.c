/**
  ******************************************************************************
  * @file    stm32f4xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4xx.h"
#include "stm32f4xx_it.h"

/* USER CODE BEGIN 0 */
#include "stm32f4xx_hal_can.h"
#include "Can.h"
#include "globvars.h"
/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern CAN_HandleTypeDef hcan1;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;

/******************************************************************************/
/*            Cortex-M4 Processor Interruption and Exception Handlers         */ 
/******************************************************************************/

/**
* @brief This function handles Non maskable interrupt.
*/
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */

  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
* @brief This function handles Hard fault interrupt.
*/
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
  }
  /* USER CODE BEGIN HardFault_IRQn 1 */

  /* USER CODE END HardFault_IRQn 1 */
}

/**
* @brief This function handles Memory management fault.
*/
void MemManage_Handler(void)
{
  /* USER CODE BEGIN MemoryManagement_IRQn 0 */

  /* USER CODE END MemoryManagement_IRQn 0 */
  while (1)
  {
  }
  /* USER CODE BEGIN MemoryManagement_IRQn 1 */

  /* USER CODE END MemoryManagement_IRQn 1 */
}

/**
* @brief This function handles Pre-fetch fault, memory access fault.
*/
void BusFault_Handler(void)
{
  /* USER CODE BEGIN BusFault_IRQn 0 */

  /* USER CODE END BusFault_IRQn 0 */
  while (1)
  {
  }
  /* USER CODE BEGIN BusFault_IRQn 1 */

  /* USER CODE END BusFault_IRQn 1 */
}

/**
* @brief This function handles Undefined instruction or illegal state.
*/
void UsageFault_Handler(void)
{
  /* USER CODE BEGIN UsageFault_IRQn 0 */

  /* USER CODE END UsageFault_IRQn 0 */
  while (1)
  {
  }
  /* USER CODE BEGIN UsageFault_IRQn 1 */

  /* USER CODE END UsageFault_IRQn 1 */
}

/**
* @brief This function handles System service call via SWI instruction.
*/
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVCall_IRQn 0 */

  /* USER CODE END SVCall_IRQn 0 */
  /* USER CODE BEGIN SVCall_IRQn 1 */

  /* USER CODE END SVCall_IRQn 1 */
}

/**
* @brief This function handles Debug monitor.
*/
void DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */

  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */

  /* USER CODE END DebugMonitor_IRQn 1 */
}

/**
* @brief This function handles Pendable request for system service.
*/
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}

/**
* @brief This function handles System tick timer.
*/
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  HAL_SYSTICK_IRQHandler();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32F4xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f4xx.s).                    */
/******************************************************************************/

/**
* @brief This function handles CAN1 TX interrupts.
*/
void CAN1_TX_IRQHandler(void)
{
  /* USER CODE BEGIN CAN1_TX_IRQn 0 */

  /* USER CODE END CAN1_TX_IRQn 0 */
  HAL_CAN_IRQHandler(&hcan1);
  /* USER CODE BEGIN CAN1_TX_IRQn 1 */

  /* USER CODE END CAN1_TX_IRQn 1 */
}

/**
* @brief This function handles CAN1 RX0 interrupts.
*/
void CAN1_RX0_IRQHandler(void)
{
  /* USER CODE BEGIN CAN1_RX0_IRQn 0 */
////
  /* USER CODE END CAN1_RX0_IRQn 0 */
  HAL_CAN_IRQHandler(&hcan1);
  /* USER CODE BEGIN CAN1_RX0_IRQn 1 */
	hcan1.pRxMsg = &msg;
  CAN_Receive_ID = hcan1.pRxMsg ->StdId;
  if(CAN_Receive_ID == 0)
  {
	  startBusy();
  }
  else
  {
	  endBusy();
  }


  CAN_Received = 1;

//	receive_CAN_interrupt(hcan1.pRxMsg->StdId);


//	if(hcan1.pRxMsg->StdId==1234)
//	{
//		CAN_Received = 1;
//	}
	switch(hcan1.pRxMsg->StdId)
		{
			case(VCU_ID):
				alive_timer_VCU = 1750;
				break;

			case(VCU_Status_ID):
				for(uint8_t i = 0; i < 5; i++)
					{
						VCU_Status.data[i]				= msg.Data[i];
					}
				break;

			case(Klemme_50_aktiv_ID):
				alive_timer_RTD = 350;
				break;

			case(VDCU_Status_ID):
				for(uint8_t i = 0; i < 4; i++)
					{
						VDCU_Status.data[i]				= msg.Data[i];
					}
				alive_timer_VDCU = 50;
				break;

			case(VDCU_Electrical_ID):
				for(uint8_t i = 0; i < 7; i++)
					{
						VDCU_Electrical.data[i]			= msg.Data[i];
					}
				break;

			case(VDCU_Wheelspeeds_ID):
				for(uint8_t i = 0; i < 8; i++)
					{
						VDCU_Wheelspeeds.data[i]		= msg.Data[i];
					}
				break;

			case(VDCU_Velocity_ID):
				for(uint8_t i = 0; i < 4; i++)
					{
						VDCU_Velocity.data[i]			= msg.Data[i];
					}
				break;

			case(VDCU_Derating_ID):
				for(uint8_t i = 0; i < 4; i++)
					{
						VDCU_Derating.data[i]			= msg.Data[i];
					}
				break;

			case(VDCU_Drivetrain_ID):
				for(uint8_t i = 0; i < 5; i++)
					{
						VDCU_Drivetrain.data[i]			= msg.Data[i];
						Update = 1;
					}
				break;

			case(VDCU_Parameter_ID):
				for(uint8_t i = 0; i < 8; i++)
					{
						VDCU_Parameter.data[i]			= msg.Data[i];
					}
				break;

			case(VDCU_CTRL_MODE_ID):
						VDCU_Control_Mode.data[0]		= msg.Data[0];
				break;

			case(VDCU_RECUP_ID):
				for(uint8_t i = 0; i < 3; i++)
					{
						VDCU_Rekuperation.data[i]		= msg.Data[i];
					}
				break;

			case(VDCU_SLIP_CTRL_ID):
				for(uint8_t i = 0; i < 7; i++)
					{
						VDCU_Slip_Control.data[i]		= msg.Data[i];
					}
				break;

			case(VDCU_TORQUE_CTRL_ID):
				for(uint8_t i = 0; i < 5; i++)
					{
						VDCU_Torque_Control.data[i]		= msg.Data[i];
					}
				break;

			case(VDCU_POWER_ID):
				for(uint8_t i = 0; i < 2; i++)
					{
						VDCU_Vehicle_Power.data[i]		= msg.Data[i];
					}
				break;

			case(VDCU_SETUP_ID):
				for(uint8_t i = 0; i < 2; i++)
					{
						Setup_Select.data[i]			= msg.Data[i];
					}
				break;

			case(AMS_voltage_ID):
				for(uint8_t i = 0; i < 8; i++)
					{
						AMS_Voltage.data[i]				= msg.Data[i];
					}
				alive_timer_BMS = 700;
				break;

			case(AMS_temp_ID):
				for(uint8_t i = 0; i < 5; i++)
					{
						AMS_Temp.data[i]				= msg.Data[i];
					}
				break;

			case(Ped_Throttle_ID):
				for(uint8_t i = 0; i < 6; i++)
					{
						PU_Throttle.data[i]				= msg.Data[i];
					}
				break;

			case(Ped_Brake_ID):
				for(uint8_t i = 0; i < 2; i++)
					{
						PU_Brake.data[i]				= msg.Data[i];
					}
				break;

			case(Ped_Stat_ID):
				for(uint8_t i = 0; i < 1; i++)
					{
						Pedalerie.data[i]				= msg.Data[i];
					}
				break;

			case(MU_front_state_ID):
				for(uint8_t i = 0; i < 2; i++)
					{
						Messbox_vorne.data[i]			= msg.Data[i];
					}
				break;

			case(MU_front_suspension_ID):
				for(uint8_t i = 0; i < 4; i++)
					{
						MU_Federweg_vorne.data[i]		= msg.Data[i];
					}
				break;

			case(MU_front_steering_ID):
				for(uint8_t i = 0; i < 2; i++)
					{
						MU_Lenkwinkel.data[i]			= msg.Data[i];
					}
				break;

			case(MU_front_brake_ID):
				for(uint8_t i = 0; i < 2; i++)
					{
						MU_Bremsdruck_vorne.data[i]		= msg.Data[i];
						MU_Brake_act = MU_Bremsdruck_vorne.data[0] | (MU_Bremsdruck_vorne.data[1]<<8);
						MU_Brake_act = MU_Brake_act * 0.1;
					}
				break;

			case(MU_front_temp_ID):
				for(uint8_t i = 0; i < 6; i++)
					{
						MU_Temperaturen_vorne.data[i]	= msg.Data[i];
					}
				break;

			case(MU_rear_state_ID):
				for(uint8_t i = 0; i < 3; i++)
					{
						Messbox_hinten.data[i]			= msg.Data[i];
					}
				break;

			case(MU_rear_suspension_ID):
				for(uint8_t i = 0; i < 4; i++)
					{
						MU_Federweg_hinten.data[i]		= msg.Data[i];
					}
				break;

			case(MU_rear_tcool_ID):
				for(uint8_t i = 0; i < 2; i++)
					{
						MU_Kuehlertemp.data[i]			= msg.Data[i];
					}
				break;

			case(MU_rear_brake_ID):
				for(uint8_t i = 0; i < 2; i++)
					{
						MU_Bremsdruck_hinten.data[i]	= msg.Data[i];
					}
				break;

			case(MU_rear_temp_ID):
				for(uint8_t i = 0; i < 6; i++)
					{
						MU_Temperaturen_hinten.data[i]	= msg.Data[i];
					}
				break;

			case(DCDC_Temp_ID):
				for(uint8_t i = 0; i < 2; i++)
					{
						DCDC_Temperaturen.data[i]		= msg.Data[i];
					}
				break;

			case(IVT_Voltage_ID):
				for(uint8_t i = 0; i < 5; i++)
					{
						IVT_Voltage.data[i]				= msg.Data[i];
					}
					Update = 1;
				break;

			case(0xB4):

				Update = 1;
						SoC_akt = msg.Data[1];
						SoC_akt = SoC_akt - 50;
						if (0 > SoC_akt)
						{
							SoC_akt = 0;
						}
						break;

			default:
				break;
		}

//	__HAL_CAN_ENABLE_IT(hcan1, CAN_IT_FMP0);
	HAL_CAN_IRQHandler(&hcan1);
	return;
  /* USER CODE END CAN1_RX0_IRQn 1 */
}

/**
* @brief This function handles TIM2 global interrupt.
*/
void TIM2_IRQHandler(void)
{
  /* USER CODE BEGIN TIM2_IRQn 0 */
	Dashboard_Status_Send_2 = 1;
  /* USER CODE END TIM2_IRQn 0 */
  HAL_TIM_IRQHandler(&htim2);
  /* USER CODE BEGIN TIM2_IRQn 1 */

  /* USER CODE END TIM2_IRQn 1 */
}

/**
* @brief This function handles TIM3 global interrupt.
*/
void TIM3_IRQHandler(void)
{
  /* USER CODE BEGIN TIM3_IRQn 0 */
	Dashboard_Status_Send_1 = 1;
  /* USER CODE END TIM3_IRQn 0 */
  HAL_TIM_IRQHandler(&htim3);
  /* USER CODE BEGIN TIM3_IRQn 1 */

  /* USER CODE END TIM3_IRQn 1 */
}

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
