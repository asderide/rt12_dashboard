/*
 * Display.c
 *
 *  Created on: 31.05.2018
 *      Author: asder
 */

#include "main.h"
#include "stm32f4xx_hal.h"
#include "Display.h"
#include "globvars.h"


void wait (unsigned char waitstates)
{
	unsigned char i=0;
	for (i=0; i<waitstates; i++)
	{
		asm("nop");				// nop: 'no operation'
	}
}

void DisplayInit (void)
{

	writeInsDisplay(0x39); 			//function set european chararacter set
	HAL_Delay(10);
	writeInsDisplay(0x39); 			//function set european chararacter set
	HAL_Delay(10);
	writeInsDisplay(0x39); 			//function set european chararacter set
	HAL_Delay(10);
	writeInsDisplay(0x39); 			//function set european chararacter set
	HAL_Delay(10);
	writeInsDisplay(0x08); 			//display off
	HAL_Delay(10);
	writeInsDisplay(0x01); 			//clear display
	HAL_Delay(10);
	writeInsDisplay(0x06); 			//entry mode set increment cursor by 1 not shifting display
	HAL_Delay(10);
	writeInsDisplay(0x17); 			//Character mode and internal power on
	HAL_Delay(10);
	writeInsDisplay(0x10);			//Cursor Move and direction
	HAL_Delay(10);
	writeInsDisplay(0x01); 			//clear display
	HAL_Delay(10);
	writeInsDisplay(0x02); 			//return home
	HAL_Delay(10);
	writeInsDisplay(0x0F); 			//display on


	unsigned char Strich_rechts[8] 			= {0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01};			// Erzeugen eines Zeichens f�r das Display laut Array-Namen
	unsigned char Strich_oben[8]			= {0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};			// Erzeugen eines Zeichens f�r das Display laut Array-Namen
	unsigned char Grad[8] 					= {0x1C, 0x14, 0x1C, 0x00, 0x00, 0x00, 0x00, 0x00};			// Erzeugen eines Zeichens f�r das Display laut Array-Namen
	unsigned char Doppelstrich_rechts[8]	= {0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03};			// Erzeugen eines Zeichens f�r das Display laut Array-Namen
	unsigned char Dreifachstrich_rechts[8]	= {0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07};			// Erzeugen eines Zeichens f�r das Display laut Array-Namen
	unsigned char Vierfachstrich_rechts[8]	= {0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F};			// Erzeugen eines Zeichens f�r das Display laut Array-Namen
	unsigned char Strich_unten[8]			= {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F};			// Erzeugen eines Zeichens f�r das Display laut Array-Namen
	unsigned char Strich_links[8]			= {0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10};

	defineCharacter(Nr_Strich_rechts, Strich_rechts);													// Definieren des entsprechenden Zeichens und definieren des Speicherortes ('0')
	defineCharacter(Nr_Doppelstrich_rechts, Doppelstrich_rechts);										// Definieren des entsprechenden Zeichens und definieren des Speicherortes ('3')
	defineCharacter(Nr_Dreifachstrich_rechts, Dreifachstrich_rechts);									// Definieren des entsprechenden Zeichens und definieren des Speicherortes ('4')
	defineCharacter(Nr_Vierfachstrich_rechts, Vierfachstrich_rechts);									// Definieren des entsprechenden Zeichens und definieren des Speicherortes ('5')
	defineCharacter(Nr_Strich_oben, Strich_oben);														// Definieren des entsprechenden Zeichens und definieren des Speicherortes ('1')
	defineCharacter(Nr_Grad, Grad);																		// Definieren des entsprechenden Zeichens und definieren des Speicherortes ('2')
	defineCharacter(Nr_Strich_unten, Strich_unten);
	defineCharacter(Nr_Strich_links, Strich_links);
}

void writeInsDisplay(char DisplayData)
{
//	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);

	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_RESET);
	GPIOD->ODR = (DisplayData<<8);

	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
	wait(16);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);

//	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);
//	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_RESET);
//	char RotData = drehen(DisplayData);
//	uint32_t DD = DisplayData;
//	GPIOD->ODR = (DD<<8);
//	HAL_Delay(10);
//	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
//	HAL_Delay(400);
//	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
}

void  writeDataDisplay (char DisplayData)
{

	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);

//	char RotData = drehen(DisplayData);
//	uint32_t DD = DisplayData;
	GPIOD->ODR = (DisplayData<<8);
//	HAL_Delay(10);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
//	HAL_Delay(30);
	wait(160);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);

}

void writeCharDisplay (char character)
{
	writeDataDisplay(character);
}

void writeStringDisplay (const char * string)
{
	do
	{
		writeDataDisplay(*string++);
	}while(*string);
}

void setPositionDisplay (char pos)
{
	writeInsDisplay(LCD_HOME_L1+pos);
}

void DisplayOnOff (char DisplayData)
{
	writeInsDisplay(0x08 + DisplayData);
}

void clearDisplay (void)
{
	writeInsDisplay(0x01);
	setPositionDisplay(LINE1);
}

void defineCharacter (unsigned char position, unsigned char *data)
{
	unsigned char i=0;
	writeInsDisplay(0x40+8*position);

	for (i=0; i<8; i++)
	{
		writeDataDisplay(data[i]);
	}
//	setPositionDisplay(LINE1);
}

//void sendDisplayTime(void)
//{
//	setPositionDisplay(LINE1);						// Display Cursor in Zeile 1 auf Position 1 setzen
//
////####### Wochentag an die ersten beiden Positionen in Zeile 1 senden + ein Leerzeichen
//
//	if(eRTC.single_time.weekday 		== Sunday)
//		writeStringDisplay("So ");
//	else if(eRTC.single_time.weekday == Monday)
//		writeStringDisplay("Mo ");
//	else if(eRTC.single_time.weekday == Tuesday)
//		writeStringDisplay("Di ");
//	else if(eRTC.single_time.weekday == Wednesday)
//		writeStringDisplay("Mi ");
//	else if(eRTC.single_time.weekday == Thursday)
//		writeStringDisplay("Do ");
//	else if(eRTC.single_time.weekday == Friday)
//		writeStringDisplay("Fr ");
//	else if(eRTC.single_time.weekday == Saturday)
//		writeStringDisplay("Sa ");
//
////####### Aktuellen Tag im Monat nach das Leerzeichen nach dem Wochentag setzen
//
//	if(eRTC.single_time.day < 10)					// Diese beiden if-Abfragen dienen dazu, die Anzahl der angezeigten Ziffern immer gleich zu lassen und n�tigenfalls eine '0' einzuf�gen
//		writeStringDisplay("0");
//	if(eRTC.single_time.day == 0)
//		writeStringDisplay("0");
//	else
//		sendDisplayDigit(eRTC.single_time.day);
//
//	writeStringDisplay(".");						// Abtrennung des Datums mit einem Punkt
//
////####### Aktuelles Monat nach den Punkt setzen
//
//	if(eRTC.single_time.month < 10)					// Diese beiden if-Abfragen dienen dazu, die Anzahl der angezeigten Ziffern immer gleich zu lassen und n�tigenfalls eine '0' einzuf�gen
//		writeStringDisplay("0");
//	if(eRTC.single_time.month == 0)
//		writeStringDisplay("0");
//	else
//	sendDisplayDigit(eRTC.single_time.month);
//
//	writeStringDisplay(".");						// Abtrennung des Datums mit einem Punkt
//
////####### Aktuelles Jahr nach den Punkt setzen
//
//	if(eRTC.single_time.year < 10)					// Diese beiden if-Abfragen dienen dazu, die Anzahl der angezeigten Ziffern immer gleich zu lassen und n�tigenfalls eine '0' einzuf�gen
//		writeStringDisplay("0");
//	if(eRTC.single_time.year == 0)
//		writeStringDisplay("0");
//	else
//	sendDisplayDigit(eRTC.single_time.year);
//
//	writeStringDisplay(" ");						// Ein Leerzeichen zwischen Datum und Uhrzeit setzen
//
////####### Aktuelle Stunde nach das Leerzeichen setzen
//
//	if(eRTC.single_time.hour < 10)					// Diese beiden if-Abfragen dienen dazu, die Anzahl der angezeigten Ziffern immer gleich zu lassen und n�tigenfalls eine '0' einzuf�gen
//		writeStringDisplay("0");
//	if(eRTC.single_time.hour == 0)
//		writeStringDisplay("0");
//	else
//	sendDisplayDigit(eRTC.single_time.hour);
//
//	writeStringDisplay(":");						// Einen Doppelpunkt als Abtrennung f�r die Uhrzeit setzen
//
////####### Aktuelle Minute nach den Doppelpunkt setzen
//
//	if(eRTC.single_time.min < 10)					// Diese beiden if-Abfragen dienen dazu, die Anzahl der angezeigten Ziffern immer gleich zu lassen und n�tigenfalls eine '0' einzuf�gen
//		writeStringDisplay("0");
//	if(eRTC.single_time.min == 0)
//		writeStringDisplay("0");
//	else
//	sendDisplayDigit(eRTC.single_time.min);
//
//	writeStringDisplay(":");
//
////####### Aktuelle Sekunde nach den Doppelpunkt setzen
//
//	if(eRTC.single_time.sec < 10)					// Diese beiden if-Abfragen dienen dazu, die Anzahl der angezeigten Ziffern immer gleich zu lassen und n�tigenfalls eine '0' einzuf�gen
//		writeStringDisplay("0");
//	if(eRTC.single_time.sec == 0)
//		writeStringDisplay("0");
//	else
//	sendDisplayDigit(eRTC.single_time.sec);
//
//	setPositionDisplay(LINE2);
//}

void DisplayMenuSoC(void)
{
	uint8_t RT10_SoC 		= RT10_input.translation.SoC.value;		// Lese den aktuellen SoC-Wert aus der entsprechenden globalen Variable in die Lokale ein
	uint8_t SoC_ten 		= 0;
	uint8_t SoC_unit 		= 0;
	uint8_t Anz_Spalten_SoC = 10;									// So viele Spalten im Display werden f�r den SoC benutzt
	uint8_t K5 				= 0b11111111;							// 'K'-Variablen indizieren die F�llung eines Feldes im Display -> 5 ist voll, 1 ist zu 1/5 gef�llt
//	uint8_t K4 				= 0b11100101;
//	uint8_t K3 				= 0b11100100;
//	uint8_t K2 				= 0b11100001;
//	uint8_t K1 				= 0b11100011;
//	uint8_t Unterstrich		= 0b01011111;

	//Zeile 2
	setPositionDisplay(LINE2);
	writeStringDisplay(" ");										// Erstes Feld wird frei gelassen
	for(uint8_t j = Anz_Spalten_SoC; j > 0; j--)
	{
		writeCharDisplay(Nr_Strich_unten);							// Schreibe das Zeichen 'waagrechter Strich unten' so oft wie Spalten ausgew�hlt sind
	}
	writeStringDisplay("                    ");						// L�sche alle Zeichen aus der restlichen Zeile
	setPositionDisplay(LINE2 + 14);
	if(!HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_6))
		writeStringDisplay("low");
	else
		writeStringDisplay("high");
	//sendDisplayDigit(RT10_input.translation.Light.value);

	//Zeile 3
	setPositionDisplay(LINE3);
	writeCharDisplay(Nr_Strich_rechts);											// Schreibe das vordefinierte Zeichen '0' -> Strich rechts
	for(uint8_t i = (RT10_SoC / (LCD_Characters-Anz_Spalten_SoC)) ; i > 0 ; i--)
	{
		writeCharDisplay(K5);										// Schreibe die Anzahl an vollst�ndig ausgef�llten Felder in Abh�ngigkeit des aktuellen SoC-Wertes in das Display
	}
	SoC_ten = RT10_SoC / 10;										// Teile aktuellen SoC durch zehn und schneide die Dezimalzahlen ab um den 'Zehner-Wert' zu erhalten
	SoC_unit = RT10_SoC - (SoC_ten * 10);							// Ziehe den Zehner-Wert vom aktuellen SoC-Wert ab (Eliminierung der 'Zehner-Stelle') um die letzte Ziffer des Wertes zu erhalten

//##### F�lle das letzte Feld entsprechend des 'Einer-Wertes' des SoC [Diese Code-Sequenz ist nur f�r eine Spaltenanzahl von 10 ausgelegt]

	if(SoC_unit > 0)
		sendDisplayDigit(SoC_unit);

	setPositionDisplay(LINE3+Anz_Spalten_SoC+1);
	writeCharDisplay(Nr_Strich_links);											// Grenze das Array f�r die SoC-Anzeige mit einem senkrechten Strich ab
	setPositionDisplay(LINE3+Anz_Spalten_SoC+2);
	sendDisplayDigit(SoC);										// Schreibe den aktuellen SoC-Wert als Zahl neben das graphische Array
	writeStringDisplay(" DRS         ");							// Einheit: Prozent (%) und l�sche den Rest der Zeile

	//Zeile 4
	setPositionDisplay(LINE4);
	writeStringDisplay(" ");

//##### Beschlie�e das SoC-Array mit einer Reihe von 'waagrechten Strichen oben', die in der Init-Funktion erzeugt und gespeichert wurden

	for(uint8_t j = Anz_Spalten_SoC; j > 0 ; j--)
	{
		writeCharDisplay(Nr_Strich_oben);										// Waagrechter Strich oben
	}
	for(uint8_t j = LCD_Characters - (1+Anz_Spalten_SoC); j > 0 ; j--)
	{
		writeStringDisplay(" ");
	}


//	sendDisplayDigit(RT10_input.translation.White_Light.value);

}

void DisplayMenuBMS(void)
{
	if(RT10_input.translation.Module_BMS.alive == TRUE)
	{
		//##### Einlesen der BMS-Nachrichten und anschlie�endes Runterbrechen auf tausendstel-Werte

		uint8_t ams_voltage_volt_min 		= 	AMS_Voltage.translation.ams_min_volt / 10000;
		uint16_t ams_voltage_millivolt_min 	= (	AMS_Voltage.translation.ams_min_volt / 10) - (ams_voltage_volt_min * 1000);

		uint8_t ams_voltage_volt_max 		= 	AMS_Voltage.translation.ams_max_volt / 10000;
		uint16_t ams_voltage_millivolt_max 	= (	AMS_Voltage.translation.ams_max_volt / 10) - (ams_voltage_volt_max * 1000);

		uint16_t ams_voltage_ges_volt 		= 	AMS_Voltage.translation.ams_accu_ges_volt / 100;
		uint16_t ams_voltage_ges_millivolt 	= 	AMS_Voltage.translation.ams_accu_ges_volt - (ams_voltage_ges_volt * 100);

		/*
 	 	 Die Darstellungen der Zellspannungen und Temperaturen sind in Volt und �C mit Nachkommastellen versehen, wodurch die umst�ndliche Berechnung
 	 	 mit den Millivolt notwendig ist
		 */

		setPositionDisplay(LINE2);
		writeStringDisplay("U:     ");
		sendDisplayDigit(ams_voltage_volt_min);
		writeStringDisplay(",");
		if(ams_voltage_millivolt_min < 100)
			{
				writeCharDisplay(48+0);
				if(ams_voltage_millivolt_min < 10)
					writeCharDisplay(48+0);
			}
		sendDisplayDigit(ams_voltage_millivolt_min);
		writeStringDisplay("V ");
		setPositionDisplay(LINE2+14);
		sendDisplayDigit(ams_voltage_volt_max);
		writeStringDisplay(",");
		if(ams_voltage_millivolt_max < 100)
			{
				writeCharDisplay(48+0);
				if(ams_voltage_millivolt_max < 10)
					writeCharDisplay(48+0);
			}
		sendDisplayDigit(ams_voltage_millivolt_max);
		writeStringDisplay("V");

		setPositionDisplay(LINE3);
		writeStringDisplay("Temp:  ");
		sendDisplayDigit(ROUND(AMS_Temp.translation.ams_min_temp/2));		// Wert wird vom BMS mit dem Faktor 2 gesendet, um Aufl�sung zu erh�hen
		writeCharDisplay(Nr_Grad);											// �C anzeigen
		writeStringDisplay("C    ");
		setPositionDisplay(LINE3+14);
		sendDisplayDigit(ROUND(AMS_Temp.translation.ams_max_temp/2));
		writeCharDisplay(Nr_Grad);											// �C anzeigen
		writeStringDisplay("C  ");
		setPositionDisplay(LINE4);
		writeStringDisplay("Total: ");
		sendDisplayDigit(ams_voltage_ges_volt);
		writeStringDisplay(",");
		if(ams_voltage_ges_millivolt < 10)
			writeCharDisplay(48+0);
		sendDisplayDigit(ams_voltage_ges_millivolt);
		writeStringDisplay("V      ");
//		setPositionDisplay(LINE4+15);
//		sendDisplayDigit(RT10_input.translation.SoC.value);
//		writeStringDisplay("%");
	}
	else
	{
		setPositionDisplay(LINE2);
		writeStringDisplay("                    ");
		setPositionDisplay(LINE3);
		writeStringDisplay("BMS ausgefallen!    ");
		setPositionDisplay(LINE4);
		writeStringDisplay("                    ");
	}
}

void DisplayMenuTractiveSystem(void)
{
//	if(RT10_input.translation.Module_VDCU.alive == TRUE)
//	{
	setPositionDisplay(LINE1);
	writeStringDisplay("IVT-U:  ");
		if(IVT_Voltage.translation.Result_Voltage < 0)
		writeStringDisplay("-");
		sendDisplayDigit(abs(ROUND(IVT_Voltage.translation.Result_Voltage/1000)));
		writeStringDisplay("V");


		setPositionDisplay(LINE1);
		writeStringDisplay("Ivt-U:  ");

		setPositionDisplay(LINE1+17);
		sendDisplayDigit(SoC_akt);
		writeStringDisplay("%");

		setPositionDisplay(LINE2);
		writeStringDisplay("Inv-T:   ");
		sendDisplayDigit(VDCU_Drivetrain.translation.Inv_l_Temp - 40);		// Einbeziehen des Offsets aus der CAN-Nachricht
		writeCharDisplay(2);
		writeStringDisplay("C   ");
		setPositionDisplay(LINE2+15);
		sendDisplayDigit(VDCU_Drivetrain.translation.Inv_r_Temp - 40);		// Einbeziehen des Offsets aus der CAN-Nachricht
		writeCharDisplay(2);
		writeStringDisplay("C   ");
		setPositionDisplay(LINE3);
		writeStringDisplay("Mot-T:   ");
		sendDisplayDigit(VDCU_Drivetrain.translation.Mot_l_Temp - 40);		// Einbeziehen des Offsets aus der CAN-Nachricht
		writeCharDisplay(2);
		writeStringDisplay("C   ");
		setPositionDisplay(LINE3+15);
		sendDisplayDigit(VDCU_Drivetrain.translation.Mot_r_Temp - 40);		// Einbeziehen des Offsets aus der CAN-Nachricht
		writeCharDisplay(2);
		writeStringDisplay("C   ");
		setPositionDisplay(LINE4);
		writeStringDisplay("Inv-State: ");
		sendDisplayDigit(VDCU_Drivetrain.translation.Inv_l_State);
		writeStringDisplay("   ");
		setPositionDisplay(LINE4+15);
		sendDisplayDigit(VDCU_Drivetrain.translation.Inv_r_State);
		writeStringDisplay("   ");
//	}
//	else
//	{
//		setPositionDisplay(LINE2);
//		writeStringDisplay("Der Inverter laeuft ");
//		setPositionDisplay(LINE3);
//		writeStringDisplay("vermutlich, aber die");
//		setPositionDisplay(LINE4);
//		writeStringDisplay("VDCU ist ausgefallen");
//	}
}

void DisplayMenuVDCU(void)
{
	if(RT10_input.translation.Module_VDCU.alive == TRUE)
	{
		setPositionDisplay(LINE2);
		writeStringDisplay("Max. Power:  ");
		sendDisplayDigit((2 * status.MinMax[Nr_VDCU][Nr_power_max] * (AMS_Voltage.translation.ams_accu_ges_volt / 100)) / 1000);
		writeStringDisplay("kW   ");
		setPositionDisplay(LINE3);
		writeStringDisplay("Max. Torque: ");
		sendDisplayDigit(status.MinMax[Nr_VDCU][Nr_torque_max]);
		writeStringDisplay("Nm   ");
		setPositionDisplay(LINE4);
		writeStringDisplay("Regler: ");
		if(!VDCU_Status.translation.recuperation_on && !VDCU_Status.translation.slipcontrol_on && !VDCU_Status.translation.torquevectoring_on )
			setPositionDisplay(LINE4+8),
			writeStringDisplay("Grundmodus  ");
		else if(VDCU_Status.translation.recuperation_on && !VDCU_Status.translation.slipcontrol_on && !VDCU_Status.translation.torquevectoring_on )
			setPositionDisplay(LINE4+8),
			writeStringDisplay("Rekuperation");
		else if(!VDCU_Status.translation.recuperation_on && VDCU_Status.translation.slipcontrol_on && !VDCU_Status.translation.torquevectoring_on )
			setPositionDisplay(LINE4+8),
			writeStringDisplay("Schlupfrgl  ");
		else if(!VDCU_Status.translation.recuperation_on && !VDCU_Status.translation.slipcontrol_on && VDCU_Status.translation.torquevectoring_on )
			setPositionDisplay(LINE4+8),
			writeStringDisplay("Momentenstrg");
		else if(VDCU_Status.translation.recuperation_on && VDCU_Status.translation.slipcontrol_on && !VDCU_Status.translation.torquevectoring_on )
			setPositionDisplay(LINE4+8),
			writeStringDisplay("Rekup+SC    ");
		else if(VDCU_Status.translation.recuperation_on && !VDCU_Status.translation.slipcontrol_on && VDCU_Status.translation.torquevectoring_on )
			setPositionDisplay(LINE4+8),
			writeStringDisplay("Rekup+TV    ");
		else if(!VDCU_Status.translation.recuperation_on && VDCU_Status.translation.slipcontrol_on && VDCU_Status.translation.torquevectoring_on )
			setPositionDisplay(LINE4+8),
			writeStringDisplay("SC+TV       ");
		else if(VDCU_Status.translation.recuperation_on && VDCU_Status.translation.slipcontrol_on && VDCU_Status.translation.torquevectoring_on )
			setPositionDisplay(LINE4+8),
			writeStringDisplay("Rekup+SC+TV ");
	}
	else
	{
		setPositionDisplay(LINE2);
		writeStringDisplay("                    ");
		setPositionDisplay(LINE3);
		writeStringDisplay("VDCU ausgefallen!   ");
		setPositionDisplay(LINE4);
		writeStringDisplay("                    ");
	}
}

void DisplayMenuDerating(void)
{
	setPositionDisplay(LINE2);
	if(VDCU_Derating.translation.derating_voltage_active)
		writeStringDisplay("Volt active!        ");
	if(VDCU_Derating.translation.derating_temp_active)
		writeStringDisplay("Temp_active!        ");
	if(VDCU_Derating.translation.derating_current_active)
		writeStringDisplay("Current_active!     ");
	if(VDCU_Derating.translation.derating_speed_active)
		writeStringDisplay("Speed active!       ");
	if(VDCU_Derating.translation.derating_celltemp_active)
		writeStringDisplay("CellTemp active!    ");
	if(VDCU_Derating.translation.derating_invtemp_active)
		writeStringDisplay("InvTemp active!     ");
	if(VDCU_Derating.translation.undervolt_active)
		writeStringDisplay("Undervolt active!   "),
		writeStringDisplay("Idc_max:            ");
		sendDisplayDigit(VDCU_Derating.translation.idc_max);
	setPositionDisplay(LINE4);
	writeStringDisplay("Faktor:             ");
	sendDisplayDigit(VDCU_Derating.translation.derating_factor);


}

void DisplayMenuPedalUnit(void)
{
	if(RT10_input.translation.Module_PU.alive == TRUE)
	{
		setPositionDisplay(LINE2);
		writeStringDisplay("                    ");
		setPositionDisplay(LINE3);
		writeStringDisplay("                    ");
		setPositionDisplay(LINE4);
		writeStringDisplay("                    ");
		setPositionDisplay(LINE2);
		writeStringDisplay("Gas 1: ");
		sendDisplayDigit(PU_Throttle.translation.gas1);
		setPositionDisplay(LINE2 + 12);
		if(PU_Throttle.translation.gas2 < 1000)
		{
			writeStringDisplay("2:   ");
			sendDisplayDigit(PU_Throttle.translation.gas2);
		}
		if(PU_Throttle.translation.gas2 >= 1000)
		{
			writeStringDisplay("2:  ");
			sendDisplayDigit(PU_Throttle.translation.gas2);
		}
		setPositionDisplay(LINE3);
		writeStringDisplay("EGas:  ");
		sendDisplayDigit(PU_Throttle.translation.egas);
		setPositionDisplay(LINE3 + 12);
		if(PU_Brake.translation.value < 1000)
		{
			writeStringDisplay("Br:  ");
			sendDisplayDigit(PU_Brake.translation.value);
		}
		if(PU_Brake.translation.value >= 1000)
		{
			writeStringDisplay("Br: ");
			sendDisplayDigit(PU_Brake.translation.value);
		}
		setPositionDisplay(LINE4);
		writeStringDisplay("BSPD:  ");
		sendDisplayDigit(Pedalerie.translation.bspd_ped);
		setPositionDisplay(LINE4 + 9);
		writeStringDisplay("Inplau: ");
		if(Pedalerie.translation.br_gas_inplau)
			sendDisplayDigit(Pedalerie.translation.br_gas_inplau);
		else if(Pedalerie.translation.gas_inplau)
			sendDisplayDigit(Pedalerie.translation.gas_inplau);
		else
			sendDisplayDigit(0);
	}
	else
	{
		setPositionDisplay(LINE2);
		writeStringDisplay("Die Pedalerie ist   ");
		setPositionDisplay(LINE3);
		writeStringDisplay("leider ausgefallen! ");
		setPositionDisplay(LINE4);
		writeStringDisplay("                    ");
	}
}

void DisplayMenuMeasUnitFront(void)
{
	if(RT10_input.translation.Module_MU_Front.alive == TRUE)
	{
		setPositionDisplay(LINE2);
		writeStringDisplay("                    ");
		setPositionDisplay(LINE3);
		writeStringDisplay("                    ");
		setPositionDisplay(LINE4);
		writeStringDisplay("                    ");
		setPositionDisplay(LINE2);
		writeStringDisplay("Federweg: ");
		sendDisplayDigit(MU_Federweg_vorne.translation.left * 0.05);
		writeStringDisplay("mm");
		setPositionDisplay(LINE2 + 15);
		sendDisplayDigit(MU_Federweg_vorne.translation.right * 0.05);
		writeStringDisplay("mm");
		setPositionDisplay(LINE3);
		writeStringDisplay("Bremsdruck: ");
		sendDisplayDigit(MU_Bremsdruck_vorne.translation.pressure / 10);
		writeStringDisplay("bar");
		setPositionDisplay(LINE4);
		writeStringDisplay("Lenkwinkel: ");
		if(MU_Lenkwinkel.translation.value < 205)
			writeStringDisplay("-"),
			sendDisplayDigit(205 - MU_Lenkwinkel.translation.value);
		else
			sendDisplayDigit(MU_Lenkwinkel.translation.value - 205);
		writeCharDisplay(2);
	}
	else
	{
		setPositionDisplay(LINE2);
		writeStringDisplay("Messbox vorne ist   ");
		setPositionDisplay(LINE3);
		writeStringDisplay("leider ausgefallen! ");
		setPositionDisplay(LINE4);
		writeStringDisplay("                    ");

	}
}

void DisplayMenuMeasUnitRear(void)
{
	if(RT10_input.translation.Module_MU_Rear.alive == TRUE)
	{
		setPositionDisplay(LINE2);
		writeStringDisplay("                    ");
		writeStringDisplay("                    ");
		writeStringDisplay("                    ");
		setPositionDisplay(LINE2);
		writeStringDisplay("Federweg: ");
		sendDisplayDigit(MU_Federweg_hinten.translation.left *0.05);
		writeStringDisplay("mm");
		setPositionDisplay(LINE2 + 15);
		sendDisplayDigit(MU_Federweg_hinten.translation.right * 0.05);
		writeStringDisplay("mm");
		setPositionDisplay(LINE3);
		writeStringDisplay("Bremsdruck: ");
		sendDisplayDigit(MU_Bremsdruck_hinten.translation.pressure / 10);
		writeStringDisplay("bar");
		setPositionDisplay(LINE4);
		writeStringDisplay("Kuehlertemp: ");
		sendDisplayDigit(MU_Kuehlertemp.translation.value);
		writeCharDisplay(Nr_Grad);
	}
	else
	{
		setPositionDisplay(LINE2);
		writeStringDisplay("Messbox hinten ist  ");
		setPositionDisplay(LINE3);
		writeStringDisplay("leider ausgefallen! ");
		setPositionDisplay(LINE4);
		writeStringDisplay("                    ");

	}

}

void DisplayMenuFederwege(void)
{
	setPositionDisplay(LINE2);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE3);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE4);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE2);
	writeStringDisplay("Federwege  li / re  ");
	setPositionDisplay(LINE3);
	writeStringDisplay("vorne:  ");
	sendDisplayDigit(MU_Federweg_vorne.translation.left * 0.05);
	writeStringDisplay("mm");
	setPositionDisplay(LINE3+15);
	sendDisplayDigit(MU_Federweg_vorne.translation.right * 0.05);
	writeStringDisplay("mm");
	setPositionDisplay(LINE4);
	writeStringDisplay("hinten: ");
	sendDisplayDigit(MU_Federweg_hinten.translation.left * 0.05);
	writeStringDisplay("mm");
	setPositionDisplay(LINE4+15);
	sendDisplayDigit(MU_Federweg_hinten.translation.right * 0.05);
	writeStringDisplay("mm");
}

void DisplayMenuBremsdruck(void)
{
	setPositionDisplay(LINE2);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE3);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE4);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE2);
	writeStringDisplay("Bremsdruck");
	setPositionDisplay(LINE3);
	writeStringDisplay("Vorne: ");
	sendDisplayDigit(MU_Bremsdruck_vorne.translation.pressure / 10);
	writeStringDisplay("bar");
	setPositionDisplay(LINE4);
	writeStringDisplay("Hinten ");
	sendDisplayDigit(MU_Bremsdruck_hinten.translation.pressure / 10);
	writeStringDisplay("bar");
}

void DisplayMenuMeasUnits(void)
{
	setPositionDisplay(LINE2);
	writeStringDisplay("Button Dashboard: ");
	sendDisplayDigit(RT10_input.translation.SBD.active);
	writeStringDisplay(" ");
	setPositionDisplay(LINE3);
	writeStringDisplay("Button links:     ");
	sendDisplayDigit(Messbox_hinten.state.SB_links);
	writeStringDisplay(" ");
	setPositionDisplay(LINE4);
	writeStringDisplay("Button rechts     ");
	sendDisplayDigit(Messbox_hinten.state.SB_rechts);
	writeStringDisplay(" ");

}

void DisplayMenuLenkwinkel(void)
{
	int16_t Lenkwinkel 			= 205 - MU_Lenkwinkel.translation.value;
	uint16_t Lenkwinkel_relativ = ((100 * abs(Lenkwinkel) / 130));
	uint16_t Lenkwinkel_fifteen	= abs(Lenkwinkel) / 15;
	uint16_t Lenkwinkel_three	= ROUND((abs(Lenkwinkel) - (Lenkwinkel_fifteen * 15)) / 3);
	uint8_t K5 					= 0b11010110;							// 'K'-Variablen indizieren die F�llung eines Feldes im Display -> 5 ist voll, 1 ist zu 1/5 gef�llt
	uint8_t K4 					= 0b11010111;
	uint8_t K3 					= 0b11011000;
	uint8_t K2 					= 0b11011001;
	uint8_t K1 					= 0b11011010;

	setPositionDisplay(LINE2);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE3);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE4);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE2);
	writeStringDisplay("Lenkwinkel");
	setPositionDisplay(LINE3);
	writeCharDisplay(Nr_Strich_rechts);

	if(Lenkwinkel > 0)
	{
		setPositionDisplay(LINE3+10);
		for(uint8_t i = Lenkwinkel_fifteen ; i > 0 ; i--)
		{
			writeCharDisplay(K5);									// Schreibe die Anzahl an vollst�ndig ausgef�llten Felder in Abh�ngigkeit des aktuellen Lenkwinkel-Wertes in das Display
		}
		if(Lenkwinkel_three >= 4)
			writeCharDisplay(K4);
		else if(Lenkwinkel_three >= 3)
			writeCharDisplay(K3);
		else if(Lenkwinkel_three >= 2)
			writeCharDisplay(K2);
		else if(Lenkwinkel_three >= 1)
			writeCharDisplay(K1);
	}

	if(Lenkwinkel < 0)
	{
		setPositionDisplay(LINE3+9-Lenkwinkel_fifteen);
		if(Lenkwinkel_three == 4)
			writeCharDisplay(Nr_Vierfachstrich_rechts);
		else if(Lenkwinkel_three == 3)
			writeCharDisplay(Nr_Dreifachstrich_rechts);
		else if(Lenkwinkel_three == 2)
			writeCharDisplay(Nr_Doppelstrich_rechts);
		else if(Lenkwinkel_three == 1)
			writeCharDisplay(Nr_Strich_rechts);
		for(uint8_t i = Lenkwinkel_fifteen ; i > 0 ; i--)
		{
			writeCharDisplay(K5);
		}
	}

	setPositionDisplay(LINE3+19);
	writeCharDisplay(K1);
	setPositionDisplay(LINE4);
	writeStringDisplay("Abs: ");
	if(MU_Lenkwinkel.translation.value < 205)
		writeStringDisplay("-"),
		sendDisplayDigit(205 - MU_Lenkwinkel.translation.value);
	else
		sendDisplayDigit(MU_Lenkwinkel.translation.value - 205);
	writeCharDisplay(2);
	setPositionDisplay(LINE4+11);
	writeStringDisplay("Rel: ");
	sendDisplayDigit(Lenkwinkel_relativ);
	writeStringDisplay("%");
}

void DisplayMenuSpeed(void)
{
	uint8_t RT10_Speed 			= VDCU_Velocity.translation.velocity/10;	// Lese den aktuellen Geschwindigkeitswert-Wert aus der entsprechenden globalen Variable in die Lokale ein
	uint16_t RT10_Distance		= VDCU_Velocity.translation.distance;
	uint8_t Speed_five 			= 0;
	uint8_t Speed_unit 			= 0;
	uint8_t Anz_Spalten_Speed 	= 18;									// So viele Spalten im Display werden f�r den SoC benutzt
	uint8_t K5 					= 0b11010110;							// 'K'-Variablen indizieren die F�llung eines Feldes im Display -> 5 ist voll, 1 ist zu 1/5 gef�llt
	uint8_t K4 					= 0b11010111;
	uint8_t K3 					= 0b11011000;
	uint8_t K2 					= 0b11011001;
	uint8_t K1 					= 0b11011010;
//	uint8_t Unterstrich			= 0b11000100;

	setPositionDisplay(LINE2);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE3);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE4);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE2);
	writeCharDisplay(Nr_Strich_rechts);											// Schreibe das vordefinierte Zeichen '0' -> Strich rechts
	for(uint8_t i = (RT10_Speed / 5) ; i > 0 ; i--)
	{
		writeCharDisplay(K5);										// Schreibe die Anzahl an vollst�ndig ausgef�llten Felder in Abh�ngigkeit des aktuellen SoC-Wertes in das Display
	}
	Speed_five = RT10_Speed / 5;										// Teile aktuellen SoC durch zehn und schneide die Dezimalzahlen ab um den 'Zehner-Wert' zu erhalten
	Speed_unit = RT10_Speed - (Speed_five * 5);							// Ziehe den Zehner-Wert vom aktuellen SoC-Wert ab (Eliminierung der 'Zehner-Stelle') um die letzte Ziffer des Wertes zu erhalten

//##### F�lle das letzte Feld entsprechend des 'Einer-Wertes' des SoC [Diese Code-Sequenz ist nur f�r eine Spaltenanzahl von 10 ausgelegt]

	if(Speed_unit >= 4)
		writeCharDisplay(K4);
	else if(Speed_unit >= 3)
		writeCharDisplay(K3);
	else if(Speed_unit >= 2)
		writeCharDisplay(K2);
	else if(Speed_unit >= 1)
		writeCharDisplay(K1);

	setPositionDisplay(LINE2+Anz_Spalten_Speed+1);
	writeCharDisplay(K1);								// Grenze das Array f�r die SoC-Anzeige mit einem senkrechten Strich ab

	setPositionDisplay(LINE3);
	writeStringDisplay("Distance: ");
	sendDisplayDigit(RT10_Distance);				// Schreibe den aktuellen SoC-Wert als Zahl neben das graphische Array
	writeStringDisplay("m");						// Einheit: Prozent (km/h) und l�sche den Rest der Zeile
	setPositionDisplay(LINE4);
	writeStringDisplay("Velocity: ");
	sendDisplayDigit(RT10_Speed);
	writeStringDisplay("km/h");

	if(RT10_input.translation.Para_OK.active && !HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_5))
		Dashboard_Status.translation.Reset_Distance_active = 1,
		RT10_input.translation.Para_OK.active = 0,
		timer_reset = 1000;
	if(timer_reset <= 0)
		Dashboard_Status.translation.Reset_Distance_active = 0;
}

void DisplayMenuAktualisierungsrate_Status(void)
{
	uint16_t timer_min = 50;
	uint16_t timer_max = 200;

	setPositionDisplay(LINE2);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE3);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE4);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE2);

	uint16_t 	value = timer_min + Rotary;	// Neuer Wert: Minimum + Rotationsdecoder

	if(RT10_input.translation.Para_OK.active && !RT10_input.translation.Para_OK.exists)
	{
		Display_Menu.display_status.timer_refresh_value			= value;
		RT10_input.translation.Para_OK.active 		= FALSE;
	}

	writeStringDisplay("Statusaktualisierung");
	setPositionDisplay(LINE3);
	writeStringDisplay("Bisher: ");
	sendDisplayDigit(Display_Menu.display_status.timer_refresh_value);
	writeStringDisplay("ms");
	setPositionDisplay(LINE4);
	writeStringDisplay("Neu: ");
	sendDisplayDigit(value);
	writeStringDisplay("ms");
}

void DisplayMenuAktualisierungsrate_Parameter(void)
{
	uint16_t timer_min = 50;
	uint16_t timer_max = 200;

	setPositionDisplay(LINE2);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE3);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE4);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE2);

	uint16_t 	value = timer_min + Rotary;	// Neuer Wert: Minimum + (Drehpoti-ADC-Wert /  Aufl�sung)

	if(RT10_input.translation.Para_OK.active && !RT10_input.translation.Para_OK.exists)
	{
		Display_Menu.display_parameter.timer_refresh_value		= value;
		RT10_input.translation.Para_OK.active 		= FALSE;
	}

	writeStringDisplay("Parameterverstellung");
	setPositionDisplay(LINE3);
	writeStringDisplay("Bisher: ");
	sendDisplayDigit(Display_Menu.display_parameter.timer_refresh_value);
	writeStringDisplay("ms");
	setPositionDisplay(LINE4);
	writeStringDisplay("Neu: ");
	sendDisplayDigit(value);
	writeStringDisplay("ms");
}

void DisplayMenuHelligkeitLEDs(void)
{
	uint16_t brightness_min = 0x00;
	uint16_t brightness_max = 16;

	setPositionDisplay(LINE2);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE3);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE4);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE2);

	uint16_t 	value 	= 17 * Rotary;	// Neuer Wert: Minimum + (Drehpoti-ADC-Wert /  Aufl�sung)

	if(RT10_input.translation.Para_OK.active && !RT10_input.translation.Para_OK.exists)
	{
		LED_brightness.value						= value;

		if(LED_brightness.value >= 0xFF)
			LED_brightness.value = 0xFF;
		if(LED_brightness.value <= 0x00)
			LED_brightness.value = 0x00;

		RT10_input.translation.Para_OK.active 		= FALSE;

		for(int i=0x12;i<0x20;i++)
			{
				WriteLEDDriver(0x02, LED_brightness.value);		//Str�me aller Ports auf 16/16, also 24mA einstellen
			}

	}

	writeStringDisplay("LED Helligkeit");
	setPositionDisplay(LINE3);
	writeStringDisplay("Aktuell: ");
	translation_current(LED_brightness.value);
	writeStringDisplay("mA");
	setPositionDisplay(LINE4);
	writeStringDisplay("Neu:     ");
	translation_current(value);
	writeStringDisplay("mA");
}

void DisplayMenuHelligkeit(void)
{
//	getLightValue();

	setPositionDisplay(LINE2);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE3);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE4);
	writeStringDisplay("                    ");

	setPositionDisplay(LINE2);
	writeStringDisplay("Debugmenue!");
	setPositionDisplay(LINE3);
	writeStringDisplay("Lichtwert: ");
	sendDisplayDigit(RT10_input.translation.Light.value);
	setPositionDisplay(LINE4);
	writeStringDisplay("Weisslicht: ");
	sendDisplayDigit(RT10_input.translation.White_Light.value);

}

void DisplayMenuAliveTimer(void)
{
	setPositionDisplay(LINE2);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE3);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE4);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE2);

	writeStringDisplay("VCU:  ");
	if(RT10_input.translation.Module_VCU.alive == TRUE)
		writeStringDisplay("alive");
	else
		writeStringDisplay("dead");

	setPositionDisplay(LINE3);
	writeStringDisplay("VDCU: ");
	if(RT10_input.translation.Module_VDCU.alive == TRUE)
		writeStringDisplay("alive");
	else
		writeStringDisplay("dead");

	setPositionDisplay(LINE4);
	writeStringDisplay("BMS:  ");
	if(RT10_input.translation.Module_BMS.alive == TRUE)
		writeStringDisplay("alive");
	else
		writeStringDisplay("dead");
}

void DisplayMenuAussentemp(void)
{
	setPositionDisplay(LINE2);
	writeStringDisplay("PreCharge-State DB: ");
	setPositionDisplay(LINE3);
	sendDisplayDigit(RT10_input.translation.preCharge.state);
	writeStringDisplay("                   ");
	setPositionDisplay(LINE4);
	writeStringDisplay("                    ");
	setPositionDisplay(LINE2);

}

void DisplayMenuSound(void)
{
	uint8_t sound = getParameterValue(21);
	setPositionDisplay(LINE2);
	writeStringDisplay("Was darf es sein?   ");
	setPositionDisplay(LINE3);
	writeStringDisplay("Melodie-Nummer: ");
	sendDisplayDigit(sound);
	writeStringDisplay("   ");
	setPositionDisplay(LINE4);
	writeStringDisplay(" State: ");
	sendDisplayDigit(20);
	writeStringDisplay("          ");
}

void DisplayMenuControlMode(void)
{
	uint8_t sw_right 	= Group;
	uint16_t value 		= 0;

	switch(sw_right)
	{
	case(0):
			setPositionDisplay(LINE2);
			writeStringDisplay("                    ");
			setPositionDisplay(LINE3);
			writeStringDisplay("    Reglerstufe     ");
			setPositionDisplay(LINE4);
			writeStringDisplay("                    ");
	break;
	case(1):
			value = getParameterValue(Nr_Reglerstufe);
			setPositionDisplay(LINE2);
			writeStringDisplay("Reglerstufe         ");
			setPositionDisplay(LINE3);
			writeStringDisplay("Aktuell: ");

			switch(status.MinMax[Nr_VDCU][Nr_Reglerstufe])
			{
			case (0):
				setPositionDisplay(LINE3+9);
				writeStringDisplay("Grundmodus ");
				break;
			case (1):
				setPositionDisplay(LINE3+9);
				writeStringDisplay("Rekup      ");
				break;
			case (2):
				setPositionDisplay(LINE3+9);
				writeStringDisplay("SC         ");
				break;
			case (3):
				setPositionDisplay(LINE3+9);
				writeStringDisplay("Rekup+SC   ");
				break;
			case (4):
				setPositionDisplay(LINE3+9);
				writeStringDisplay("TV         ");
				break;
			case (5):
				setPositionDisplay(LINE3+9);
				writeStringDisplay("Rekup+TV   ");
				break;
			case (6):
				setPositionDisplay(LINE3+9);
				writeStringDisplay("SC+TV      ");
				break;
			case (7):
				setPositionDisplay(LINE3+9);
				writeStringDisplay("Rekup+SC+TV");
				break;
			default:
				setPositionDisplay(LINE3+9);
				writeStringDisplay("Grundmodus");
				break;
			}

			setPositionDisplay(LINE4);
			writeStringDisplay("Soll:               ");

			switch(value)
			{
			case (0):
				setPositionDisplay(LINE4+9);
				writeStringDisplay("Grundmodus ");
				break;
			case (1):
				setPositionDisplay(LINE4+9);
				writeStringDisplay("Rekup      ");
				break;
			case (2):
				setPositionDisplay(LINE4+9);
				writeStringDisplay("SC         ");
				break;
			case (3):
				setPositionDisplay(LINE4+9);
				writeStringDisplay("Rekup+SC   ");
				break;
			case (4):
				setPositionDisplay(LINE4+9);
				writeStringDisplay("TV         ");
				break;
			case (5):
				setPositionDisplay(LINE4+9);
				writeStringDisplay("Rekup+TV   ");
				break;
			case (6):
				setPositionDisplay(LINE4+9);
				writeStringDisplay("SC+TV      ");
				break;
			case (7):
				setPositionDisplay(LINE4+9);
				writeStringDisplay("Rekup+SC+TV");
				break;
			default:
				setPositionDisplay(LINE4+9);
				writeStringDisplay("Grundmodus ");
				break;
			}
		setPositionDisplay(LINE4 + 19);
		sendDisplayDigit(VDCU_Status.translation.calibration_state);
	break;
	default:
		setPositionDisplay(LINE2);
		writeStringDisplay("Schalterstellung    ");
		setPositionDisplay(LINE3);
		writeStringDisplay("nicht belegt!       ");
		setPositionDisplay(LINE4);
		writeStringDisplay("Naechster Versuch   ");
		break;
	}
}

void DisplayMenuRekuperation(void)
{
	if(VDCU_Status.translation.recuperation_on)
	{
		uint8_t sw_right = Group;
		uint16_t value = 0;

		switch(sw_right)
		{
		case(0):
				setPositionDisplay(LINE2);
				writeStringDisplay("                    ");
				setPositionDisplay(LINE3);
				writeStringDisplay("    Rekuperation    ");
				setPositionDisplay(LINE4);
				writeStringDisplay("                    ");
			break;
		case(1):
					value = getParameterValue(Nr_current_min);
				setPositionDisplay(LINE2);
				writeStringDisplay("Rekuperationsstrom  ");
				setPositionDisplay(LINE3);
				writeStringDisplay("Aktuell: -");
				sendDisplayDigit(status.MinMax[Nr_VDCU][Nr_current_min]);
				writeStringDisplay("A        ");
				setPositionDisplay(LINE4);
				writeStringDisplay("Soll: -");
				sendDisplayDigit(value);
				writeStringDisplay("A          ");
				setPositionDisplay(LINE4 + 18);
				sendDisplayDigit(VDCU_Status.translation.calibration_state);
			break;
		case(2):
					value = getParameterValue(Nr_MaxRecTrqBrake);
				setPositionDisplay(LINE2);
				writeStringDisplay("Bremsmoment         ");
				setPositionDisplay(LINE3);
				writeStringDisplay("Aktuell: ");
				sendDisplayDigit(status.MinMax[Nr_VDCU][Nr_MaxRecTrqBrake]);
				writeStringDisplay("Nm       ");
				setPositionDisplay(LINE4);
				writeStringDisplay("Soll: ");
				sendDisplayDigit(value);
				writeStringDisplay("Nm          ");
				setPositionDisplay(LINE4 + 18);
				sendDisplayDigit(VDCU_Status.translation.calibration_state);
			break;
		case(3):
					value = getParameterValue(Nr_MaxRecTrqEgas);
				setPositionDisplay(LINE2);
				writeStringDisplay("Schleppmoment       ");
				setPositionDisplay(LINE3);
				writeStringDisplay("Aktuell: ");
				sendDisplayDigit(status.MinMax[Nr_VDCU][Nr_MaxRecTrqEgas]);
				writeStringDisplay("Nm        ");
				setPositionDisplay(LINE4);
				writeStringDisplay("Soll: ");
				sendDisplayDigit(value);
				writeStringDisplay("Nm           ");
				setPositionDisplay(LINE4 + 18);
				sendDisplayDigit(VDCU_Status.translation.calibration_state);
			break;
		default:
			setPositionDisplay(LINE2);
			writeStringDisplay("Schalterstellung    ");
			setPositionDisplay(LINE3);
			writeStringDisplay("nicht belegt!       ");
			setPositionDisplay(LINE4);
			writeStringDisplay("Naechster Versuch   ");
			break;
		}
	}
	else
	{
		setPositionDisplay(LINE2);
		writeStringDisplay("    Rekuperation    ");
		setPositionDisplay(LINE3);
		writeStringDisplay("     ist leider     ");
		setPositionDisplay(LINE4);
		writeStringDisplay("    deaktiviert     ");
	}
}

void DisplayMenuSchlupfkontrolle(void)
{
	if(VDCU_Status.translation.slipcontrol_on)
	{
		uint8_t sw_right 	= Group;
		int16_t value 		= 0;
		float offset;

		switch(sw_right)
		{
		case(0):
				setPositionDisplay(LINE2);
				writeStringDisplay("                    ");
				setPositionDisplay(LINE3);
				writeStringDisplay("  Schlupfkontrolle  ");
				setPositionDisplay(LINE4);
				writeStringDisplay("                    ");
			break;
		case(1):
					value = getParameterValue(Nr_slipRef);
				setPositionDisplay(LINE2);
				writeStringDisplay("slipRef             ");
				setPositionDisplay(LINE3);
				writeStringDisplay("Aktuell: ");
				sendDisplayDecimal(Nr_slipRef, status.MinMax[Nr_VDCU][Nr_slipRef]);
				writeStringDisplay("       ");
				setPositionDisplay(LINE4);
				writeStringDisplay("Soll: ");
				sendDisplayDecimal(Nr_slipRef, value);
				writeStringDisplay("          ");
				setPositionDisplay(LINE4 + 18);
				sendDisplayDigit(VDCU_Status.translation.calibration_state);
			break;
		case(2):
					value = getParameterValue(Nr_PT_T);
				setPositionDisplay(LINE2);
				writeStringDisplay("PT");
				writeCharDisplay(Nr_Strich_unten);
				writeStringDisplay("T                ");
				setPositionDisplay(LINE3);
				writeStringDisplay("Aktuell: ");
				sendDisplayDecimal(Nr_PT_T, status.MinMax[Nr_VDCU][Nr_PT_T]);
				writeStringDisplay("       ");
				setPositionDisplay(LINE4);
				writeStringDisplay("Soll: ");
				sendDisplayDecimal(Nr_PT_T, value);
				writeStringDisplay("          ");
				setPositionDisplay(LINE4 + 18);
				sendDisplayDigit(VDCU_Status.translation.calibration_state);
			break;
		case(3):
					value = getParameterValue(Nr_PID_P);
				setPositionDisplay(LINE2);
				writeStringDisplay("PID");
				writeCharDisplay(Nr_Strich_unten);
				writeStringDisplay("P               ");
				setPositionDisplay(LINE3);
				writeStringDisplay("Aktuell: ");
				sendDisplayDigit(status.MinMax[Nr_VDCU][Nr_PID_P]);
				writeStringDisplay("           ");
				setPositionDisplay(LINE4);
				writeStringDisplay("Soll: ");
				sendDisplayDigit(value);
				writeStringDisplay("           ");
				setPositionDisplay(LINE4 + 18);
				sendDisplayDigit(VDCU_Status.translation.calibration_state);
			break;
		case(4):
					value = getParameterValue(Nr_PID_I);
				setPositionDisplay(LINE2);
				writeStringDisplay("PID");
				writeCharDisplay(Nr_Strich_unten);
				writeStringDisplay("I               ");
				setPositionDisplay(LINE3);
				writeStringDisplay("Aktuell: ");
				sendDisplayDigit((status.Faktor[Nr_PID_I] * status.MinMax[Nr_VDCU][Nr_PID_I]));
				writeStringDisplay("       ");
				setPositionDisplay(LINE4);
				writeStringDisplay("Soll: ");
				sendDisplayDigit(status.Faktor[Nr_PID_I] * value);
				writeStringDisplay("          ");
				setPositionDisplay(LINE4 + 18);
				sendDisplayDigit(VDCU_Status.translation.calibration_state);
			break;
		case(5):
					value = getParameterValue(Nr_PID_D);
				offset = (5.12 / status.Faktor[Nr_PID_D]);
				setPositionDisplay(LINE2);
				writeStringDisplay("PID");
				writeCharDisplay(Nr_Strich_unten);
				writeStringDisplay("D               ");
				setPositionDisplay(LINE3);
				writeStringDisplay("Aktuell: ");
				sendDisplayDecimal(Nr_PID_D, (status.MinMax[Nr_VDCU][Nr_PID_D] - offset));
				writeStringDisplay("       ");
				setPositionDisplay(LINE4);
				writeStringDisplay("Soll: ");
				sendDisplayDecimal(Nr_PID_D, (value - offset));
				writeStringDisplay("          ");
				setPositionDisplay(LINE4 + 18);
				sendDisplayDigit(VDCU_Status.translation.calibration_state);
			break;
		case(6):
					value = getParameterValue(Nr_PID_N);
				setPositionDisplay(LINE2);
				writeStringDisplay("PID");
				writeCharDisplay(Nr_Strich_unten);
				writeStringDisplay("N               ");
				setPositionDisplay(LINE3);
				writeStringDisplay("Aktuell: ");
				sendDisplayDigit(status.MinMax[Nr_VDCU][Nr_PID_N]);
				writeStringDisplay("          ");
				setPositionDisplay(LINE4);
				writeStringDisplay("Soll: ");
				sendDisplayDigit(value);
				writeStringDisplay("            ");
				setPositionDisplay(LINE4 + 18);
				sendDisplayDigit(VDCU_Status.translation.calibration_state);
			break;
		case(7):
					value = getParameterValue(Nr_PID_Kb);
				setPositionDisplay(LINE2);
				writeStringDisplay("PID");
				writeCharDisplay(Nr_Strich_unten);
				writeStringDisplay("Kb              ");
				setPositionDisplay(LINE3);
				writeStringDisplay("Aktuell: ");
				sendDisplayDecimal(Nr_PID_D, status.MinMax[Nr_VDCU][Nr_PID_Kb]);
				writeStringDisplay("        ");
				setPositionDisplay(LINE4);
				writeStringDisplay("Soll: ");
				sendDisplayDecimal(Nr_PID_D, value);
				writeStringDisplay("            ");
				setPositionDisplay(LINE4 + 18);
				sendDisplayDigit(VDCU_Status.translation.calibration_state);
			break;
		default:
			setPositionDisplay(LINE2);
			writeStringDisplay("Schalterstellung    ");
			setPositionDisplay(LINE3);
			writeStringDisplay("nicht belegt!       ");
			setPositionDisplay(LINE4);
			writeStringDisplay("Naechster Versuch   ");
			break;
		}
	}
	else
	{
		setPositionDisplay(LINE2);
		writeStringDisplay("  Schlupfkontrolle  ");
		setPositionDisplay(LINE3);
		writeStringDisplay("     ist leider     ");
		setPositionDisplay(LINE4);
		writeStringDisplay("    deaktiviert     ");
	}
}

void DisplayMenuMomentensteuerung(void)
{
	if(VDCU_Status.translation.torquevectoring_on)
	{
		uint8_t sw_right = Group;
		uint16_t value = 0;

		switch(sw_right)
		{
		case(0):
			setPositionDisplay(LINE2);
			writeStringDisplay("                    ");
			setPositionDisplay(LINE3);
			writeStringDisplay(" Momentensteuerung  ");
			setPositionDisplay(LINE4);
			writeStringDisplay("                    ");
			break;
		case(1):
			value = getParameterValue(Nr_Mdiff_min);
			setPositionDisplay(LINE2);
			writeStringDisplay("Mdiff");
			writeCharDisplay(Nr_Strich_unten);
			writeStringDisplay("min           ");
			setPositionDisplay(LINE3);
			writeStringDisplay("Aktuell: ");
			sendDisplayDigit(status.MinMax[Nr_VDCU][Nr_Mdiff_min]);
			writeStringDisplay("        ");
			setPositionDisplay(LINE4);
			writeStringDisplay("Soll: ");
			sendDisplayDigit(value);
			writeStringDisplay("           ");
			setPositionDisplay(LINE4 + 18);
			sendDisplayDigit(VDCU_Status.translation.calibration_state);
			break;
		case(2):
			value = getParameterValue(Nr_Mdiff_max);
			setPositionDisplay(LINE2);
			writeStringDisplay("Mdiff");
			writeCharDisplay(Nr_Strich_unten);
			writeStringDisplay("max           ");
			setPositionDisplay(LINE3);
			writeStringDisplay("Aktuell: ");
			sendDisplayDigit(status.MinMax[Nr_VDCU][Nr_Mdiff_max]);
			writeStringDisplay("        ");
			setPositionDisplay(LINE4);
			writeStringDisplay("Soll: ");
			sendDisplayDigit(value);
			writeStringDisplay("           ");
			setPositionDisplay(LINE4 + 18);
			sendDisplayDigit(VDCU_Status.translation.calibration_state);
			break;
		case(3):
			value = getParameterValue(Nr_vel_low);
			setPositionDisplay(LINE2);
			writeStringDisplay("vel");
			writeCharDisplay(Nr_Strich_unten);
			writeStringDisplay("low             ");
			setPositionDisplay(LINE3);
			writeStringDisplay("Aktuell: ");
			sendDisplayDigit(status.MinMax[Nr_VDCU][Nr_vel_low]);
			writeStringDisplay("           ");
			setPositionDisplay(LINE4);
			writeStringDisplay("Soll: ");
			sendDisplayDigit(value);
			writeStringDisplay("            ");
			setPositionDisplay(LINE4 + 18);
			sendDisplayDigit(VDCU_Status.translation.calibration_state);
			break;
		case(4):
			value = getParameterValue(Nr_vel_high);
			setPositionDisplay(LINE2);
			writeStringDisplay("vel");
			writeCharDisplay(Nr_Strich_unten);
			writeStringDisplay("high            ");
			setPositionDisplay(LINE3);
			writeStringDisplay("Aktuell: ");
			sendDisplayDigit(status.MinMax[Nr_VDCU][Nr_vel_high]);
			writeStringDisplay("           ");
			setPositionDisplay(LINE4);
			writeStringDisplay("Soll: ");
			sendDisplayDigit(value);
			writeStringDisplay("           ");
			setPositionDisplay(LINE4 + 18);
			sendDisplayDigit(VDCU_Status.translation.calibration_state);
			break;
		case(5):
			value = getParameterValue(Nr_trq_low);
			setPositionDisplay(LINE2);
			writeStringDisplay("trq");
			writeCharDisplay(Nr_Strich_unten);
			writeStringDisplay("low             ");
			setPositionDisplay(LINE3);
			writeStringDisplay("Aktuell: ");
			sendDisplayDigit(status.MinMax[Nr_VDCU][Nr_trq_low]);
			writeStringDisplay("          ");
			setPositionDisplay(LINE4);
			writeStringDisplay("Soll: ");
			sendDisplayDigit(value);
			writeStringDisplay("            ");
			setPositionDisplay(LINE4 + 18);
			sendDisplayDigit(VDCU_Status.translation.calibration_state);
			break;
		case(6):
			value = getParameterValue(Nr_trq_high);
			setPositionDisplay(LINE2);
			writeStringDisplay("trq");
			writeCharDisplay(Nr_Strich_unten);
			writeStringDisplay("high            ");
			setPositionDisplay(LINE3);
			writeStringDisplay("Aktuell: ");
			sendDisplayDigit(status.MinMax[Nr_VDCU][Nr_trq_high]);
			writeStringDisplay("          ");
			setPositionDisplay(LINE4);
			writeStringDisplay("Soll: ");
			sendDisplayDigit(value);
			writeStringDisplay("            ");
			setPositionDisplay(LINE4 + 18);
			sendDisplayDigit(VDCU_Status.translation.calibration_state);
			break;
		default:
			setPositionDisplay(LINE2);
			writeStringDisplay("Schalterstellung    ");
			setPositionDisplay(LINE3);
			writeStringDisplay("nicht belegt!       ");
			setPositionDisplay(LINE4);
			writeStringDisplay("Naechster Versuch   ");
			break;
		}
	}
	else
	{
		setPositionDisplay(LINE2);
		writeStringDisplay("  Torque Vectoring  ");
		setPositionDisplay(LINE3);
		writeStringDisplay("     ist leider     ");
		setPositionDisplay(LINE4);
		writeStringDisplay("    deaktiviert     ");
	}
}

void DisplayMenuFahrzeugleistung(void)
{
	uint8_t sw_right = Group;
	uint16_t value = 0;

	switch(sw_right)
	{
	case(0):
			setPositionDisplay(LINE2);
			writeStringDisplay("                    ");
			setPositionDisplay(LINE3);
			writeStringDisplay("  Fahrzeugleistung  ");
			setPositionDisplay(LINE4);
			writeStringDisplay("                    ");
	break;
	case(1):
			value = getParameterValue(Nr_power_max);
			setPositionDisplay(LINE2);
			writeStringDisplay("Maximaler Strom     ");
			setPositionDisplay(LINE3);
			writeStringDisplay("Aktuell: ");
			sendDisplayDigit(status.MinMax[Nr_VDCU][Nr_power_max]);
			writeStringDisplay("A      ");
			setPositionDisplay(LINE4);
			writeStringDisplay("Soll: ");
			sendDisplayDigit(value);
			writeStringDisplay("A          ");
			setPositionDisplay(LINE4 + 18);
			sendDisplayDigit(VDCU_Status.translation.calibration_state);
		break;
	case(2):
			value = getParameterValue(Nr_torque_max);
			setPositionDisplay(LINE2);
			writeStringDisplay("Maximales Drehmoment");
			setPositionDisplay(LINE3);
			writeStringDisplay("Aktuell: ");
			sendDisplayDigit(status.MinMax[Nr_VDCU][Nr_torque_max]);
			writeStringDisplay("Nm      ");
			setPositionDisplay(LINE4);
			writeStringDisplay("Soll: ");
			sendDisplayDigit(value);
			writeStringDisplay("Nm         ");
			setPositionDisplay(LINE4 + 18);
			sendDisplayDigit(VDCU_Status.translation.calibration_state);
		break;
	default:
		setPositionDisplay(LINE2);
		writeStringDisplay("Schalterstellung    ");
		setPositionDisplay(LINE3);
		writeStringDisplay("nicht belegt!       ");
		setPositionDisplay(LINE4);
		writeStringDisplay("Naechster Versuch   ");
		break;
	}

}

void DisplayMenuSetupauswahl(void)
{
	uint8_t sw_right = Group;
	uint16_t value = 0;

	switch(sw_right)
	{
	case(0):
			setPositionDisplay(LINE2);
			writeStringDisplay("                    ");
			setPositionDisplay(LINE3);
			writeStringDisplay("    Setupauswahl    ");
			setPositionDisplay(LINE4);
			writeStringDisplay("                    ");
	break;
	case(1):
			value = getParameterValue(Nr_Setup);
			setPositionDisplay(LINE2);
			writeStringDisplay("Setup-Nummer        ");
			setPositionDisplay(LINE3);
			writeStringDisplay("Aktuell: ");
			sendDisplayDigit(status.MinMax[Nr_VDCU][Nr_Setup]);
			writeStringDisplay("        ");
			setPositionDisplay(LINE4);
			writeStringDisplay("Soll: ");
			sendDisplayDigit(value);
			writeStringDisplay("            ");
			setPositionDisplay(LINE4 + 18);
			sendDisplayDigit(VDCU_Status.translation.calibration_state);
		break;
	default:
		setPositionDisplay(LINE2);
		writeStringDisplay("Schalterstellung    ");
		setPositionDisplay(LINE3);
		writeStringDisplay("nicht belegt!       ");
		setPositionDisplay(LINE4);
		writeStringDisplay("Naechster Versuch   ");
		break;
	}
}

void DisplayMenuFahrzeugstatus_1(void)
{
	uint8_t sw_right = Group;

	switch(sw_right)
	{
	case(0):
			setPositionDisplay(LINE2);
			writeStringDisplay("                    ");
			setPositionDisplay(LINE3);
			writeStringDisplay("   Fahrzeugstatus   ");
			setPositionDisplay(LINE4);
			writeStringDisplay("       Teil 1       ");
			break;
	case(1):
			DisplayMenuBMS();
		break;
	case(2):
			DisplayMenuTractiveSystem();
		break;
	case(3):
			DisplayMenuVDCU();
		break;
	case(4):
			DisplayMenuPedalUnit();
		break;
	case(5):
			DisplayMenuMeasUnitFront();
		break;
	case(6):
			DisplayMenuMeasUnitRear();
		break;
	case(7):
			DisplayMenuSpeed();
		break;
	case(8):
			DisplayMenuLenkwinkel();
		break;
	case(9):
			DisplayMenuFederwege();
		break;
	case(10):
			DisplayMenuBremsdruck();
		break;
	case(11):
			DisplayMenuMeasUnits();
		break;
	default:
			setPositionDisplay(LINE2);
			writeStringDisplay("Schalterstellung    ");
			setPositionDisplay(LINE3);
			writeStringDisplay("nicht belegt.       ");
			setPositionDisplay(LINE4);
			writeStringDisplay("Naechster Versuch!  ");
		break;
	}
}

void DisplayMenuFahrzeugstatus_2(void)
{
	uint8_t sw_right = Group;

	switch(sw_right)
	{
	case(0):
			setPositionDisplay(LINE2);
			writeStringDisplay("                    ");
			setPositionDisplay(LINE3);
			writeStringDisplay("   Fahrzeugstatus   ");
			setPositionDisplay(LINE4);
			writeStringDisplay("       Teil 2       ");
			break;
	case(1):
			DisplayMenuHelligkeit();
			break;
	case(2):
			DisplayMenuAussentemp();
			break;
	case(3):
			DisplayMenuAliveTimer();
			break;
	default:
			setPositionDisplay(LINE2);
			writeStringDisplay("Schalterstellung    ");
			setPositionDisplay(LINE3);
			writeStringDisplay("nicht belegt.       ");
			setPositionDisplay(LINE4);
			writeStringDisplay("Naechster Versuch!  ");
		break;
	}
}

void DisplayMenuDashboardSetup(void)
{
	uint8_t sw_right = Group;

	switch(sw_right)
	{
	case(0):
			setPositionDisplay(LINE2);
			writeStringDisplay("                    ");
			setPositionDisplay(LINE3);
			writeStringDisplay("     Dashboard      ");
			setPositionDisplay(LINE4);
			writeStringDisplay("                    ");
			break;
	case(1):
			DisplayMenuAussentemp();
			break;
	case(2):
			DisplayMenuHelligkeitLEDs();
			break;
	case(3):
			DisplayMenuAktualisierungsrate_Status();
			break;
	case(4):
			DisplayMenuAktualisierungsrate_Parameter();
			break;
	default:
		setPositionDisplay(LINE2);
		writeStringDisplay("Schalterstellung    ");
		setPositionDisplay(LINE3);
		writeStringDisplay("nicht belegt!       ");
		setPositionDisplay(LINE4);
		writeStringDisplay("Naechster Versuch   ");
		break;
	}
}

void DisplayMenuETKMode(void)
{
	if(VDCU_Status.translation.calibration_mode == 0)
	{
		setPositionDisplay(LINE2);
		writeStringDisplay("Lieber Fahrer, die  ");
		setPositionDisplay(LINE3);
		writeStringDisplay("VDCU m�chte aktuell ");
		setPositionDisplay(LINE4);
		writeStringDisplay("nichts von dir h�ren");
	}
	if(VDCU_Status.translation.calibration_mode == 2)
		setPositionDisplay(LINE2),
		writeStringDisplay("Lieber Fahrer, das  "),
		setPositionDisplay(LINE3),
		writeStringDisplay("CANoe hat zur Zeit  "),
		setPositionDisplay(LINE4),
		writeStringDisplay("mehr Macht als du!!!");
	else if(VDCU_Status.translation.calibration_mode == 3)
		setPositionDisplay(LINE3),
		writeStringDisplay("       fertsch      ");
}

void sendDisplayDigit(uint32_t value)
{
/*
 * Runterbrechen eines vorzeichenunbehafteten Integer-Wertes einer beliebigen Variable mit maximal 32bit auf die einzelnen Ziffern, da diese
 * separat an das Display �bertragen werden m�ssen.
 * Anschlie�endes �bertragen der Ziffern an das Display.
*/
	uint8_t value_unit 				= 0;
	uint8_t value_ten 				= 0;
	uint8_t value_hundred 			= 0;
	uint8_t value_thousand 			= 0;
	uint8_t value_tenthousand 		= 0;
	uint8_t value_hundredthousand 	= 0;
	uint8_t value_million 			= 0;
	uint8_t value_tenmillion		= 0;

	if(value >= 10000000)
	{
		value_tenmillion = value / 10000000;
		value -= value_tenmillion * 10000000;
		writeCharDisplay(48+value_tenmillion);

		if(value < 1000000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 100000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 1000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 100)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10)
		{
			writeCharDisplay(48+0);
		}
	}

	if(value >= 1000000)
	{
		value_million = value / 1000000;
		value -= value_million * 1000000;
		writeCharDisplay(48+value_million);

		if(value < 100000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 1000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 100)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10)
		{
			writeCharDisplay(48+0);
		}
	}

	if(value >= 100000)
	{
		value_hundredthousand = value / 100000;
		value -= value_hundredthousand * 100000;
		writeCharDisplay(48+value_hundredthousand);

		if(value < 10000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 1000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 100)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10)
		{
			writeCharDisplay(48+0);
		}
	}

	if(value >= 10000)
	{
		value_tenthousand = value / 10000;
		value -= value_tenthousand * 10000;
		writeCharDisplay(48+value_tenthousand);

		if(value < 1000)
		{
			writeCharDisplay(48+0);
		}
		if(value < 100)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10)
		{
			writeCharDisplay(48+0);
		}
	}

	if(value >= 1000)
	{
		value_thousand = value / 1000;
		value -= value_thousand * 1000;
		writeCharDisplay(48+value_thousand);

		if(value < 100)
		{
			writeCharDisplay(48+0);
		}
		if(value < 10)
		{
			writeCharDisplay(48+0);
		}
	}

	if(value >= 100)
	{
		value_hundred = value / 100;
		value -= value_hundred * 100;
		writeCharDisplay(48+value_hundred);

		if(value < 10)
		{
			writeCharDisplay(48+0);
		}
	}

	if(value >= 10)
		value_ten = value / 10,
		value -= value_ten * 10,
		writeCharDisplay(48+value_ten);

	if(value >= 0)
		value_unit = value,
		writeCharDisplay(48+value_unit);
}

void sendDisplayDecimal(uint8_t parameter, uint16_t value)
{
	float parameter_value_real	 		= value * status.Faktor[parameter];
	uint16_t parameter_value_unit		= parameter_value_real;
	uint16_t parameter_value_decimal 	= value - (parameter_value_unit / status.Faktor[parameter]);
	uint16_t faktor_inverse				= (1 / status.Faktor[parameter]);

	sendDisplayDigit(parameter_value_unit);
	writeStringDisplay(",");
	switch(faktor_inverse)
	{
		case (1000):
			if(parameter_value_decimal < 1000)
			{
				writeCharDisplay(48+0);
				if(parameter_value_decimal < 100)
				{
					writeCharDisplay(48+0);
					if(parameter_value_decimal < 10)
						writeCharDisplay(48+0);
				}
			}
		sendDisplayDigit(parameter_value_decimal);
		break;

		case(100):
			if(parameter_value_decimal < 100)
			{
				writeCharDisplay(48+0);
				if(parameter_value_decimal < 10)
					writeCharDisplay(48+0);
			}
			sendDisplayDigit(parameter_value_decimal);
		break;

		case(10):
			if(parameter_value_decimal < 10)
					writeCharDisplay(48+0);
			sendDisplayDigit(parameter_value_decimal);
		break;

		case(1):
				sendDisplayDigit(parameter_value_decimal);
		break;

		default: sendDisplayDigit(parameter_value_decimal);
		break;
	}

}
